<!-- Design Nester - %asset_name% Start -->

<div id="footer-js" class="d-none" style="display: none!important;">
    <script defer src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    
    <script defer src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>
    
    
    %globals_asset_contents_raw:653895%
    <script defer src="./?a=604904?v=%globals_asset_version:604904%"></script>   <!--@@ /dev/lib.js @@-->
    <script defer src="./?a=604796?v=%globals_asset_version:604796%"></script>   <!--@@ /dev/custom.js @@-->
    

    <script runat="server">
        if( typeof loadFooterScript != 'undefined' && loadFooterScript.length >=1 ){
            for(var i = 0; i < loadFooterScript.length; i++){
                print(loadFooterScript[i]);
                print('\n');
            }
        }
    </script>
    <!-- Google Analytics Code -->
    <!--@@ <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    
      ga('create', 'UA-371870-51', 'auto');
      ga('require', 'GTM-KF8K7C8');
      ga('send', 'pageview');
    

     (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    
      ga('create', 'UA-371870-3', 'auto');
      ga('send', 'pageview');
    @@-->
        <!-- Hotjar Analytics Code -->
    <!--@@     $(document).ready(function(){
            (function(h,o,t,j,a,r){
                h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
                h._hjSettings={hjid:307817,hjsv:5};
                a=o.getElementsByTagName('head')[0];
                r=o.createElement('script');r.async=1;
                r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
                a.appendChild(r);
                
            })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
            
            $('input[type="text"], textarea, select').attr('data-hj-masked','');
        });
    @@-->
    <!-- Monsido Analytics Code -->
    <!--@@ var _monsido = _monsido || [];
    _monsido.push(['_setDomainToken', 'fwpHAwsQ8nBwX-DVgeLoHg']);
    _monsido.push(['_withStatistics', 'true']);

    </script> @@-->
    
</div><!--#footer-js-->
    
<!-- Design Nester - %asset_name% End --></link>
<!-- Design UAT Nester - %asset_name% Start -->

<div id="footer-js" class="d-none" style="display: none!important;">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>
    
    
    %globals_asset_contents_raw:653895%
    
    <script defer src="./?a=625827?v=%globals_asset_version:625827%"></script>   <!--@@ /uat/lib.js @@-->
    <script defer src="./?a=625826?v=%globals_asset_version:625826%"></script>   <!--@@ /uat/custom.js @@-->
    
    
    <script runat="server">
        if( typeof loadFooterScript != 'undefined' && loadFooterScript.length >=1 ){
            for(var i = 0; i < loadFooterScript.length; i++){
                print(loadFooterScript[i]);
                print('\n');
            }
        }
    </script>
    <script>FontAwesomeConfig = { searchPseudoElements: true,observeMutations: true };</script>
    <script  src="https://kit.fontawesome.com/911881cb04.js" crossorigin="anonymous"></script>
    
</div>

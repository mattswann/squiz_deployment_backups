var menudebug = false;
$("header .list-group-item.dropdown").has('> a .arrow-down-icon').each(function () {
	
	$(this).click(function (event) {
		event.preventDefault();
		event.stopPropagation();
		if(menudebug===true){console.log('top menu clicked', this);}
		
		
		
		if($(this).hasClass('selected')){
			//close all
			$("header .list-group-item.dropdown").removeClass("selected");
			$("header .list-group-item.dropdown .dropdown-menu").removeClass("show");
			$("header .list-group-item.dropdown a > span.arrow-up-icon").removeClass('arrow-up-icon').addClass('arrow-down-icon');
			return false;
		} else {
			if($(this).find('.sub-menu-level2 > .dropdown-item').length && !$('header  .menu-mobile-on').length){
				$('body').click();
			} else {
				$("header .list-group-item.dropdown").removeClass("selected");
				$("header .list-group-item.dropdown .dropdown-menu").removeClass("show");
				$("header .list-group-item.dropdown a > span.arrow-up-icon").removeClass('arrow-up-icon').addClass('arrow-down-icon');
			}
			$("header .list-group-item.dropdown").removeClass("selected");
			$(this).addClass('selected');
			$(this).find('.dropdown-menu.dropdown-menu-wide').addClass('show');
			$(this).find('a > span.arrow-down-icon').removeClass('arrow-down-icon').addClass('arrow-up-icon');
		}
		
		//select first item in level 2
		if($(this).find('.sub-menu-level2 > .dropdown-item').length && !$('header .menu-mobile-on').length){
			$(this).find('.sub-menu-level2 > .dropdown-item').get(0).click();
		}
		setTimeout(function(){ 
		    $('nav .arrow-up-icon').find('svg.svg-inline--fa').attr('role','presentation');
    		$('nav .arrow-down-icon').find('svg.svg-inline--fa').attr('role','presentation');
            $('nav .mobile-arrow-right-icon').find('svg.svg-inline--fa').attr('role', 'presentation');
		}, 300);
		
		
		return false;
	});
	$(this).find('a').click(function (event) {
		if(menudebug===true){console.log('em');}
		if(!$('header  .menu-mobile-on').length){
			event.preventDefault();
		}
	})
});

$("header .list-group-item.dropdown .sub-menu-level2 .dropdown-item").each(function () {
	$(this).click(function (event) {
		
		
		if( $('header  .menu-mobile-on').length < 1 ){
			if(menudebug===true){console.log('sub-menu-level2 menu clicked on desktop');}
			event.preventDefault();
			event.stopPropagation();
			
			$('.sub-menu-level2 .dropdown-item').removeClass('selected');
			
			$(this).addClass('selected');
			var toolsSection = $(this).attr('rel');
			if(menudebug===true){console.log('tools section: ' + toolsSection);}
			$('.level1-tools div[data-tools-rel!="' + toolsSection + '"]').addClass('d-none');
			$('.level1-tools div[data-tools-rel="' + toolsSection + '"]').removeClass('d-none');
			
			var level3_container = $(this).parents('.dropdown.selected').find('.sub-menu-level3-container');
			
			level3_container.html('');
			$('nav .arrow-up-icon').find('svg.svg-inline--fa').attr('role','presentation');
    		$('nav .arrow-down-icon').find('svg.svg-inline--fa').attr('role','presentation');
            $('nav .mobile-arrow-right-icon').find('svg.svg-inline--fa').attr('role', 'presentation');
			$(this).find('.sub-menu-level3').clone().appendTo( level3_container );
			$("header .sub-menu-level3-container  a").on('click', function (event) {
				if(menudebug===true){console.log('.sub-menu-level3 a');}
				event.stopPropagation();
				return true;
			})
			return false;
		} else {
			event.stopPropagation();
			if(menudebug===true){console.log('sub-menu-level2 menu clicked on mobile');}
			return true; // follow a link clicked
		}
	});
})



$("header .list-group-item.dropdown .dropdown-menu").click(function () {
	if(menudebug===true){console.log('dropdown-menu');}
	return false;
})


//  CLOSE THE MENU 
$('body, .mbtn-close, .btn-mainmenu-close').click(function () {
	if(menudebug===true){console.log('body clicked');}
	$("header .list-group-item.dropdown").removeClass("selected");
	$("header .list-group-item.dropdown .dropdown-menu").removeClass("show");
	$("header .list-group-item.dropdown a > span.arrow-up-icon").removeClass('arrow-up-icon').addClass('arrow-down-icon');
	$('header nav').removeClass('menu-mobile-on').addClass('d-none');
	$('body .arrow-up-icon').find('svg.svg-inline--fa').attr('role','presentation');
	$('body .arrow-down-icon').find('svg.svg-inline--fa').attr('role','presentation');
    $('nav .mobile-arrow-right-icon').find('svg.svg-inline--fa').attr('role', 'presentation');
	$('.btn-mainmenu-open').removeClass('d-none').addClass('d-block')
	$('.btn-mainmenu-close').hide();
});

$(window).resize(function(){
	if(!is_scrolling){
		$('body').click();
	}
});


// ESCAPE KEY
$(document).keyup(function (e) {
	if(menudebug===true){console.log('escape');}
	var KEYCODE_ESC = 27;
	if (e.keyCode == KEYCODE_ESC) {
		$('body').click();
	}
});


// mobile menu

$('.btn-mainmenu-open').click(function(event){
	if(menudebug===true){console.log('btn-mainmenu-open');}
	event.preventDefault();
	event.stopPropagation();
	$('header nav').removeClass('d-none').addClass('menu-mobile-on');
	//$('header nav .ul-menu').toggleClass('d-flex');
	
	$(this).addClass('d-none').removeClass('d-block')
	$('.btn-mainmenu-close').show();
})


// replace Squiz brackets
$('header .list-group-item a').each(function(){
    var linkText = $(this).html();
    linkText = linkText.replace(/\(/g, '');
    linkText = linkText.replace(/\)/g, '');
    $(this).html(linkText);
});



var t, l = (new Date()).getTime();
var is_scrolling = false;

$(window).scroll(function(){
    var now = (new Date()).getTime();
    
    if(now - l > 400){
        $(this).trigger('scrollStart');
        l = now;
    }
    
    clearTimeout(t);
    t = setTimeout(function(){
        $(window).trigger('scrollEnd');
    }, 300);
});

$(window).on('scrollStart', function(){
    if(menudebug===true){console.log('scrollStart');}
	is_scrolling = true;
});

$(window).on('scrollEnd', function(){
    if(menudebug===true){console.log('scrollEnd');}
	is_scrolling = false;
});

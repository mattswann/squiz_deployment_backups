! function(a) {
    a.fn.wrapInTag = function(opts) {
		var tag = opts.tag || 'strong'
			, words = opts.words || []
			, regex = RegExp(words.join('|'), 'gi') 
			, replacement = '<'+ tag +'>$&</'+ tag +'>';
			
			return this.html(function() {
				if (regex.test(a(this).html())){
					a(this).addClass('alert alert-warning');
				} 
				return a(this).html().replace(regex, replacement);
			});
	}
	
    a.fn.DAWizard = function(environment) {
        var thePreviousStep = theCurrentStep = DAWizard.config.firstStep;
        if("undefined"==typeof environment){ environment = ""; }
        var theCurrentStepLabel = ""
            , theDevType
            , $myInterval
            , $environment = environment
            , $exclusionPolicies = []
			, $exclusionZones = []
			, $removeValFromZones = []
			, $LayerJSON = {}
			, $devTypes = []
			, $replaceThis = false
			, $localException = false
			, $GISerror = false
			, $flagHistoric = false
			, $highlightHidden = false
    		, $selectedDevType
    		, $currentPolicy = ''
    		, $LatLon
    		, DAWizardData = false
    		, ValuationNumber = false;
    		
        this.debug = function() {
            // return false;
            return DAWizard.config.debug;
        },
        
        this.init = function() {
            var that = this;
            that.toggleElements('body .spinme','show', 0);
            function loadQuestions(tCS){
                if(that.loadQuestions(tCS)){
                    // a('div.da-step:eq(0)').css({"display":"block"});
                    a('div.da-step#'+DAWizard.config.firstStep).css({"display":"block"});
                    
                    a('body .da-step#daw-wizard').remove();
                    
                    return true;
                }
                return false;
            }
            // clear button
            a('body').on('click', '#ih-address button[data-action="clear"]',function(e){
				e.preventDefault();
				$LayerJSON['LGA17'] = (function () { return; })();
				$LayerJSON['LGA17url'] = (function () { return; })();
				$LayerJSON['Zone23'] = (function () { return; })();
				$LayerJSON['ZoneMeaning23'] = (function () { return; })();
				$LayerJSON['Policy23'] = (function () { return; })();
				$LayerJSON['PolicyMeaning23'] = (function () { return; })();
				$LayerJSON['HeritageClassOne59'] = (function () { return; })();
				$LayerJSON['HeritageClassTwo59'] = (function () { return; })();
				$replaceThis = false;
				$flagHistoric = false;
				a("body #ih-address .daw-continue").remove();
				a("#ih-address .fetching.input-group-btn i.loader-xs").addClass('d-none');
				a('#ih #address-lookup').val('').focus();
			});
            // continue button
            a('body').on("click", '#dawizard .daw-continue button[type="submit"]', function(e){
                e.preventDefault();
                if(a(this).data('final-step') == true) {
                    // hide search here instead
                    
                    that.toggleElements('body label[for="address-lookup"]', 'hide', 0);
			        that.toggleElements('body #address-lookup', 'hide',0);
			        that.toggleElements('body .fetching', 'hide', 0);
                }
                that.toggleElements('body .spinme','show', 0); 
                var currentStep = a(this).parents('div.da-step').attr('id');
                var processStep = that.processStep(currentStep);
                if(processStep === false){
                    
                    that.errorResponse(currentStep,'error');
                } else {
                    a.when(that.proceedStep(processStep,currentStep,'next')).done(function(x){
                        // that.toggleElements('body .spinme','hide', 0);
                        0!=that.debug() && console.log('- toggle run from .daw-continue click');
                    });
                }
                a('body button.btn svg.svg-inline--fa').attr('role','presentation');
                that.toggleElements('.btn.btn-back-link', 'show',0);
            }),
            // restart button
            a('body').on('click', '#dawizard .daw-restart button[type="submit"]', function(e){
                e.preventDefault();
                that.toggleElements('body .spinme','show', 0);
                0!=that.debug() && console.warn('restarting the Wizard...');
                a('form[id^="formstep_"]')[0].reset();
                // clear IH container
                a('body div.da-step:visible, body div.da-answer:visible').fadeOut(250, 'linear', function(){
                    a('body #ih').html('');
                    
                });
                var loadQ = loadQuestions(theCurrentStep);
                if(loadQ){
                    a('body .page-heading .h1').text(DAWizard.logic[DAWizard.config.firstStep].section.heading);
                    that.toggleElements('body #dawizard .btn-back-link', 'show', 0);
                    that.toggleElements('body .spinme', 'hide', 0);
                    0!=that.debug() && console.log("hidden at 115");
                    that.toggleElements('.btn.btn-back-link', 'hide',0);
                    a('body button.btn svg.svg-inline--fa').attr('role','presentation');
                }
            }),
            a('body').on('click', '#ih-address a[data-link-action="daw-contactcouncil"]', function(e){
                // proceed to answer
                e.preventDefault();
                that.toggleElements('body .spinme','show', 0);
                0!=that.debug() && console.log('force loading contact council');
                // that.proceedStep('daw-wizard', 'daw-wizard','next');
                a.when( that.proceedStep('daw-wizard', 'daw-wizard','next') ).done(function(x){
                    that.toggleElements('body .spinme','show', 0);
                    0!=that.debug() && console.log('- toggle run from data-link-action="daw-contactcouncil" click');
                });
            }),
            // clear selection from current step before going back a step
            a("body").on("change",'#dawizard .da-step input', function() {
                var thisStep = a(this).parents('div.da-step').attr('id');
                that.errorResponse(thisStep,'clear');
            }),
            // run get previous step
            a('body').on("click", '#dawizard .btn-back-link',function(e){
                e.preventDefault();
                that.previousQuestion();
            }),
            // trunacate long form with toggle function
            a("body").on("click", '#dawizard .da-step #more_options', function(c) {
                c.preventDefault();
                if(a('body #dawizard .da-step div[aria-hidden="true"][data-toggle-more="true"]').length > 0 ){
                    a('body #dawizard .da-step div[data-toggle-more="true"]').attr('aria-hidden','false');
                    that.toggleElements("body #dawizard .da-step div[data-toggle-more=\"true\"]", "show", 400);
                    a(this).addClass('expanded').text(DAWizard.messages.lessSearchOptions);
                }else{
                    a('body #dawizard .da-step div[data-toggle-more="true"]').attr('aria-hidden','true');
                    that.toggleElements("body #dawizard .da-step div[data-toggle-more=\"true\"]", "hide", 400);
                    a(this).removeClass('expanded').text(DAWizard.messages.moreSearchOptions);
                }
                a('body button#more_options svg.svg-inline--fa').attr('role','presentation');
            });
            function wizardjqXHR() {
                return a.ajax( {
                    url: DAWizard.config.urlJSON,
                    type: 'GET',
                    dataType: 'text'
                    // dataType: 'json'
                })
                .done(function(data) {
                    data = data.replace(/\t/g,'')
                        .replace(/\[\\/g,'[')
                        .replace(/, \\"/g,', "')
                        .replace(/,\\"/g,', "')
                        .replace(/\\",/g,'",')
                        .replace(/\\"\]/g, '"]')
                        .replace(/",}/g, '"}')
                        .replace(/},}}/g, '}}}');
                    // 0!=that.debug()&&(console.log(data));
                    
                    // data = JSON.stringify(data);
        			data = JSON.parse(data);
        			
        			0!=that.debug()&&(console.log(data));
        			if(typeof data['dev-type']['error'] != undefined ){
        				0!=that.debug()&&(console.log('DA WIzard data loaded'));
        				a("body #ih-address .input-group i.loader-xs").addClass('d-none');
        				a('body #address-lookup').attr('disabled',false).attr('placeholder','Search for property location');
        				a('body button.btn svg.svg-inline--fa').attr('role','presentation');
        				$devTypes = data;
        			} else {
        			    // error in JSON
        			    var $errorcode = 'e0x500.918';
        			    a.confirm({
        					title: 'Failed to retrieve data',
        					content: '<p>'+data.error+': '+$errorcode+'</p><p>Please try again or if this error continues, <a target="_blank" title="Contact DPTI" href="' + sapp.defaults.url + '' + sapp.defaults.contactPage + '">contact us quoting the error.</a></p>',
        					animation: 'none',
        					theme: 'supervan',
        					animationClose: 'top',
        					columnClass: 'col-10 col-md-8 col-lg-6 col-lg-offset-3 col-md-offset-2 col-offset-1',
        					containerFluid: false, // this will add 'container-fluid' instead of 'container'
        				// 	backgroundDismiss: false, // this will just close the modal
        					backgroundDismiss: function(){
                                return 'cancelAction'; // force rejection of terms
                            },
        					buttons: {
        						cancelAction: {
        							text: 'Close',
        							action: function () {
        								window.location.reload();
        							}
        						}
        					}
        				});
        			}
    		    })
    		    .fail(function(xhr, status, error) {
    				
    				var $errorcode = 'e0x500.215';
    				a("body #ih-address .input-group i.loader-xs.d-none").removeClass('d-none');
                    a('body #address-lookup').attr('disabled','disabled').attr('placeholder','X Unable to load Development approval wizard data');
    				console.error($errorcode+":"+error);
    				a.confirm({
    					title: status+' '+xhr['status'],
    					content: '<p>'+error+': '+$errorcode+'</p><p>Please try again or if this error continues, <a target="_blank" title="Contact DPTI" href="' + sapp.defaults.url + '' + sapp.defaults.contactPage + '">contact us quoting the error.</a></p>',
    					animation: 'none',
    					theme: 'supervan',
    					animationClose: 'top',
    					columnClass: 'col-10 col-md-8 col-lg-6 col-lg-offset-3 col-md-offset-2 col-offset-1',
    					containerFluid: false, // this will add 'container-fluid' instead of 'container'
    				// 	backgroundDismiss: false, // this will just close the modal
    					backgroundDismiss: function(){
                            return 'cancelAction'; // force rejection of terms
                        },
    					buttons: {
    						cancelAction: {
    							text: 'Close',
    							action: function () {
    								// close
    							}
    						}
    					}
    				});
    				xhr = JSON.stringify(xhr, null, 2);
    				console.error( "JSON request error: " + xhr );
    			});
            }
            
            if(DAWizardData==false){
                a.when( wizardjqXHR() ).done(function( x ) {
                    0!=that.debug()&&(console.warn('when wizardjqXHR'));
                    a('body button.btn svg.svg-inline--fa').attr('role','presentation');
                    DAWizardData = true;
                });
                // get council details from csv;				
    			0!=that.debug()&&(console.log('loading council lookups'));
    			var counciljqXHR = a.ajax(
			    {
    				type: "GET",
    				url: DAWizard.config.urlCouncilCSV,
    				dataType: "text"
			    })
                .done(function(data) {
                    //data = that.processCSVdata(data);
					data = that.csvJSON(data);
					$LayerJSON['councilCSV'] = data;
					
					0!=that.debug()&&(console.table($LayerJSON['councilCSV']));
                })
        		.fail(function(jqXHR, textStatus, errorThrown) {
        		    console.log( "error retrieving CSV file\n"+textStatus+": "+ errorThrown );
					//console.log(JSON.stringify(jqXHR, null, 2));
					a.confirm({
						title: 'Council Lookup error: '+errorThrown,
						content: '<p>'+textStatus+' '+jqXHR['status']+' - e0x400.740</p><p>Please try again or if this error continues, <a target="_blank" title="Contact DPTI" href="' + sapp.defaults.url + '' + sapp.defaults.contactPage + '">contact us quoting the error.</a></p>',
						animation: 'none',
						theme: 'supervan',
						animationClose: 'top',
						columnClass: 'col-10 col-md-8 col-lg-6 col-lg-offset-3 col-md-offset-2 col-offset-1',
						containerFluid: false, // this will add 'container-fluid' instead of 'container'
				// 		backgroundDismiss: false, // this will just close the modal
						backgroundDismiss: function(){
                            return 'cancelAction'; // force rejection of terms
                        },
						buttons: {
							cancelAction: {
								text: 'Close',
								action: function () {
									// close
								}
							}
						}
					});
        		});
            }
            var loadQ = loadQuestions(theCurrentStep);
            return loadQ;
        }, 
        
        this.loadQuestions = function(step) {
            var that = this,
                questionContent = '';
            0!=that.debug() && console.group('loadQuestion');
            0!=that.debug() && console.log('[deprecated] step: ' + step);
            that.toggleElements('body .spinme','show', 0);
            // get DAWizard.logic[step].section ["heading", "subHeading", "buttonType", "errorMessage"]
            
            a.each(DAWizard.logic, function (i, value) {
                if(value.section.type == 'question'){
                    0!=that.debug() && console.log('Load all questions - ' + i);
                    
                    questionContent += '<div class="da-step container px-sm-0 px-0 mb-7 mb-md-8" id="' + i + '">';
                    questionContent += '    <div class="row no-gutters" data-title-attr="' + DAWizard.logic[i].section.heading + '">';
                    questionContent += '        <div class="col-12 col-md-8 border-0 mb-0 pb-0">';
                    questionContent += '            <p class="col-12 mb-6 p-0">' + DAWizard.logic[i].section.subHeading + '</p>';
                    questionContent += '            <form class="inactive-form" id="formstep_' + i + '">';
                    questionContent += '            <filedset><legend class="sr-only" aria-label="' + DAWizard.logic[i].section.heading + '" id="vh-devs' + i + '" >' + DAWizard.logic[i].section.heading + '</legend>';
                    questionContent += '            <div class="form-group row no-gutters" role="radiogroup" aria-labelledby="vh-devs' + i + '">';
                    questionContent += '                <span role="alert" class="py-4 col-12 error-message d-none" data-error-for="' + i + '">' + DAWizard.logic[i].section.errorMessage + '</span>';
                    
                    
                    // DAWizard.logic[step].questions
                    var hiddenCount = 0;
                    a.each(value.questions, function (ii, valueb) {
                        // type: "radio",
                        // label: "External/outdoor works",
                        // helpText: "For example, adding a pool, deck or shed",
                        // hidden: false,
                        // seperator: false,
                        // questions: "daw-actionext",
                        // answers: false
                        
                        switch(valueb.type){
                            default:
                            case 'radio':
                                if(valueb.hidden){
                                    hiddenCount++;
                                    // if(hiddenCount==1){
                                    //     questionContent += '            <div class="col-12">';
                                    //     questionContent += '                <button id="more_options" class="btn btn-link pl-0 mb-3 ml-3 ml-md-0" type="button">' + DAWizard.messages.moreSearchOptions + '</button>';
                                    //     questionContent += '            </div>';
                                    // }
                                }
                                
                                if(valueb.seperator){
                                    questionContent += '                <div class="col-12 p-0 font-weight-bold mb-4 mt-1">Or</div>';
                                }
                                if(valueb.hidden && hiddenCount==1){
                                    questionContent += '            <div aria-hidden="true" data-toggle-more="true" class="d-none p-0 w-100"><div class="row no-gutters">';
                                }
                                questionContent += '                <div class="radio col-12 mb-4 mt-0';
                                
                                if(DAWizard.logic[i].section.columns==2){
                                    questionContent += ' col-md-6';
                                }
                                
                                questionContent += '">';
                                questionContent += '                    <input id="' + ii + '" name="' + i + '" type="radio">';
                                questionContent += '                    <label for="' + ii + '" class="mb-0">' + valueb.label + '</label>';
                                if(valueb.helpText){
                                    questionContent += '                    <small class="small-text-top form-text text-muted mt-n1">' + valueb.helpText + '</small>';
                                }
                                questionContent += '                </div>';
                                
                                break;
                            case 'select':
                            case 'checkbox':
                            case 'text':
                                // not needed yet
                                break
                        }
                        0!=that.debug() && console.log('question[' + ii + ']: ' + JSON.stringify(valueb, null,2));
                    });
                    if(hiddenCount>0){
                        questionContent += '            </div></div>';
                        // if(valueb.hidden && valueb.type == 'radio'){
                            // if(hiddenCount==1){
                                questionContent += '            <div class="col-12">';
                                questionContent += '                <button id="more_options" class="btn btn-link pl-0 mb-3 ml-3 ml-md-0" type="button">' + DAWizard.messages.moreSearchOptions + '</button>';
                                questionContent += '            </div>';
                            // }
                        // }
                    }
                    questionContent += '            </div>';
                    
                    switch(value.section.buttonType){
                        default:
                        case 'continue':
                            questionContent += DAWizard.config.continueButton;
                            break;
                        case 'restart':
                            questionContent += DAWizard.config.restartButton
                            break;
                    }
                    
                    questionContent += '           </fieldset></form>';
                    questionContent += '        </div>';
                    questionContent += '    </div>';
                    questionContent += '</div>';
                }
                a('body button.btn svg.svg-inline--fa').attr('role','presentation');
            });
                
            a('.wizard-questions').html(questionContent);
            a('body button.btn svg.svg-inline--fa').attr('role','presentation');
            
            0!=that.debug() && console.groupEnd('loadQuestion');
            return true;
        },
        
        this.processStep = function(current) {
            var that = this;
            that.toggleElements('body .spinme','show', 0);
            0!=that.debug() && console.group('processStep');
            0!=that.debug() && console.log('current: '+ current);
            var locateStep = DAWizard.logic[current];
            0!=that.debug() && console.log('locateStep: '+ locateStep);
            if(!locateStep){
                0!=that.debug() && console.groupEnd('processStep');
                return false;
            } else {
                // get selected answer
                if (a('#'+current+' input[name="'+current+'"]').is(':radio')) {
                    var currentAnswer = a('#'+current+' input[name="'+current+'"]:checked').attr('id');
                    if(typeof currentAnswer != 'undefined' && currentAnswer!=''){
                        0!=that.debug() && console.log('currentAnswer (radio): '+ currentAnswer);
                        0!=that.debug() && console.groupEnd('processStep');
                        return currentAnswer;
                    }
                    
                } else if (a('#'+current+' input[name="'+current+'"]').is("select")) {
                    var currentAnswer = a('#'+current+' input[name="'+current+'"] option:selected');
                    if(currentAnswer.val()!=''){
                        currentAnswer = currentAnswer.attr('id');
                        0!=that.debug() && console.log('currentAnswer (select): '+ currentAnswer);
                        0!=that.debug() && console.groupEnd('processStep');
                        return currentAnswer;
                    }
                    
                } else {
                    var currentAnswer = a('#'+current+' input[name="'+current+'"]');
                    if(currentAnswer.val()!=''){
                        currentAnswer = currentAnswer.attr('id');
                        0!=that.debug() && console.log('currentAnswer: '+ currentAnswer);
                        0!=that.debug() && console.groupEnd('processStep');
                        return currentAnswer;
                    }
                }
                0!=that.debug() && console.groupEnd('processStep');
                return false;
            }
        },
        
        this.proceedStep = function(currentAnswer,currentStep,action) {
            var that = this;
            0!=that.debug() && console.group('proceedStep');
            that.toggleElements('body .spinme','show', 0);
            if ( action == 'next') {
                var currentObj = DAWizard.logic[currentStep].questions[currentAnswer];
            }
            
            if(typeof action == 'undefined' || action != "previous"){
                action = 'next';
            }
            
            that.errorResponse(currentStep,'clear');
            0!=that.debug() && console.log('currentObj: '+ JSON.stringify(currentObj, null, 2));
            if(typeof currentObj == 'undefined' && action == 'next'){
                that.errorResponse(currentStep,'error');
            } else if ( action == 'next') {
                if(currentObj.questions == false){
                    // get answer page
                    0!=that.debug() && console.log('show answer');
                    if(currentObj.answers=='daw-contactcouncil'){
                        that.toggleElements('body #dawizard .btn-back-link', 'hide', 0);
                    }
                    a('body div.da-step:visible, body div.da-answer:visible').fadeOut(250, 'linear', function(){ 
                        // show next
                        var newTitle = a('body div#' + currentObj.answers + ' > div.row').data('title-attr');
                        0!=that.debug() && console.log('new heading[1]: ' + newTitle);
                        a('body .page-heading .h1').text(newTitle);
                        
                        theCurrentStep = currentObj.answers;
                        thePreviousStep = currentStep;
                        
                        if(theCurrentStep == 'daw-wizard' && currentAnswer != 'daw-wizard'){
                            // run address lookup
                            theCurrentStepLabel = currentObj.label;
                            theDevType = currentAnswer;
                            a.when( that.initDAWizard() ).done(function( x ) {
                                0!=that.debug() && console.log('initDAWizard: finally loaded');
                                that.toggleElements('body .spinme','fadeOut', 0);
                                0!=that.debug() && console.log("hidden at 484");
                                a('body div.da-answer#' + currentObj.answers).fadeIn(250,'linear', function(){
                                    0!=that.debug() && console.log('theCurrentStep: ' + theCurrentStep + ' - currentAnswer: ' + currentAnswer);
                                    a('body button#more_options svg.svg-inline--fa').attr('role','presentation');
                                });
                            });
                        } else {
                            that.toggleElements('body .spinme','fadeOut', 0);
                            0!=that.debug() && console.log("hidden at 492");
                            a('body div.da-answer#' + currentObj.answers).fadeIn(250,'linear', function(){
                                0!=that.debug() && console.log('theCurrentStep: ' + theCurrentStep + ' - currentAnswer: ' + currentAnswer);
                                
                            });
                        }
                            
                    });
                } else {
                    0!=that.debug() && console.log('get next Question: ' + currentObj.questions);
                    a('div.da-step:visible').fadeOut(250, 'linear', function(){ 
                        var newTitle = a('div.da-step#' + currentObj.questions + ' > div.row').data('title-attr');
                        0!=that.debug() && console.log('new heading[2]: ' + newTitle);
                        a('.page-heading .h1').text(newTitle);
                        a('div.da-step#' + currentObj.questions).fadeIn(250,'linear', function(){
                            theCurrentStep = currentObj.questions;
                            thePreviousStep = currentStep;
                            that.toggleElements('body .spinme','hide', 0);
                            0!=that.debug() && console.log("hidden at 507");
                        });
                        
                    });
                }
            } else {
                0!=that.debug() && console.log('go to previous question: ' + thePreviousStep + ' [theCurrentStep: ' + theCurrentStep + ']');
                if(thePreviousStep === DAWizard.config.firstStep){
                    that.toggleElements('.btn.btn-back-link', 'hide',0);
                }
                that.clearFormElements(theCurrentStep);
                // check previous is not current
                if(theCurrentStep == thePreviousStep){
                    var stopIterating = false;
                    
                    $.each(DAWizard.logic, function (i, value) {
                        if(stopIterating==false){
                            if(typeof value == 'object'){
                                $.each(value, function (ia, valuea) {
                                    if(typeof valuea == 'object'){
                                        if(ia=='questions'){
                                            $.each(valuea, function (ib, valueb) {
                                                if(typeof valueb == 'object'){
                                                    $.each(valueb, function (ic, valuec) {
                                                        if(stopIterating==false){
                                                            if(typeof valuec == 'object'){
                                                                // should not nest this level
                                                                DAWizardRun.errorResponse(thePreviousStep,'error');
                                                            }else{
                                                                if(thePreviousStep==valuec){
                                                                    if(getPrevious(i, ic, valuec) == true){
                                                                        stopIterating = true;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
                function getPrevious(parentQuestion,type,current) {
                    if(current != false && type == 'questions'){
                        // 0!=that.debug() && console.log('previous question shown: ' + thePreviousStep);
                        if (a('#'+parentQuestion+' input[name="'+parentQuestion+'"]').is(':radio')) {
                            var currentAnswer = a('#'+parentQuestion+' input[name="'+parentQuestion+'"]:checked').attr('id');
                            if(typeof currentAnswer != 'undefined' && currentAnswer!=''){
                                
                                thePreviousStep = parentQuestion;
                                0!=that.debug() && console.log('previous question shown: ' + thePreviousStep);
                                return true;
                            }
                            
                        } else if (a('#'+parentQuestion+' input[name="'+parentQuestion+'"]').is("select")) {
                            var currentAnswer = a('#'+parentQuestion+' input[name="'+parentQuestion+'"] option:selected');
                            if(currentAnswer.val()!=''){
                                currentAnswer = currentAnswer.attr('id');
                                thePreviousStep = parentQuestion;
                                return true;
                            }
                            
                        } else {
                            var currentAnswer = a('#'+parentQuestion+' input[name="'+parentQuestion+'"]');
                            if(currentAnswer.val()!=''){
                                currentAnswer = currentAnswer.attr('id');
                                thePreviousStep = parentQuestion;
                                return true;
                            }
                        }
                    }
                };
                
                a('div.da-step:visible, div.da-answer:visible').fadeOut(250, 'linear', function(){ 
                    var newTitle = a('div.da-step#' + thePreviousStep + ' > div.row').data('title-attr');
                    0!=that.debug() && console.log('new heading[3]: ' + newTitle);
                    a('.page-heading .h1').text(newTitle);
                    a('div.da-step#' + thePreviousStep).fadeIn(250,'linear', function(){
                        // previous step shown
                        theCurrentStep = thePreviousStep;
                        // clear IH container
                        a('body #ih').html('');
                        0!=that.debug() && console.log('previous question shown: ' + thePreviousStep);
                    });
                });
            }
            0!=that.debug() && console.groupEnd('proceedStep');
            
            return true;
        },
        
        this.errorResponse = function(current,action) {
            var that = this;
            0!=that.debug() && console.group('errorResponse');
            0!=that.debug() && console.warn('errorResponse: ' + current + '\naction: ' + action);
            switch(action){
                default:
                case 'error':
                    a('#' + current + ' .error-message').removeClass('d-none').parents('div.form-group').addClass('form-group-error');
                    if(typeof current != 'undefined'){
                        that.toggleElements('body .spinme','hide', 0);
                        0!=that.debug() && console.log("hidden at 617");
                    }
                    break;
                case 'clear':
                    a('#' + current + ' .error-message').addClass('d-none').parents('div.form-group').removeClass('form-group-error');
                    break;
            }
            
            0!=that.debug() && console.groupEnd('errorResponse');
        },
        
        this.previousQuestion = function() {
            var that = this;
            that.toggleElements('body .spinme','show', 0);
            0!=that.debug() && console.group('previousQuestion');
            0!=that.debug() && console.log('theCurrentStep: ' + theCurrentStep + '\nthePreviousStep: ' + thePreviousStep);
            
            if(thePreviousStep != 'daw-wizard'){
                // find current/visible step or answer
                var current = a('.da-step:visible, .da-answer:visible'),
                    currentID = current.attr('id'),
                    currentClass = current.attr('class');
                if(currentClass.indexOf("da-step") >= 0){
                    currentClass = "da-step";
                    if(currentID == DAWizard.config.firstStep ){
                        0!=that.debug() && console.groupEnd('previousQuestion');
                        that.toggleElements('.btn.btn-back-link', 'hide',0);
                        window.history.back();
                        return true;
                    }
                    
                    
                } else if(currentClass.toLowerCase().indexOf("da-answer") >= 0){
                    currentClass = "da-answer";
                }
                0!=that.debug() && console.log('currentQuestion ID: ' + currentID + '\ncurrentQuestion Class: ' + currentClass);
                0!=that.debug() && console.groupEnd('previousQuestion');
                // that.proceedStep(currentID,theCurrentStep,'previous');
                a.when( that.proceedStep(currentID,theCurrentStep,'previous') ).done(function(x){
                    that.toggleElements('body .spinme','hide', 0);
                });
            } else {
                a('body div.da-step:visible, body div.da-answer:visible').fadeOut(250, 'linear', function(){
                    that.toggleElements('body #daw-wizard', 'show', 200);
                    a.when( that.initDAWizard() ).done(function( x ) {
                        0!=that.debug() && console.log('initDAWizard: finally loaded');
                        
                    });
                });
            }
        },
        
        this.clearFormElements = function(groupID) {
            a("#"+groupID).find(':input').each(function() {
                switch(this.type) {
                    case 'password':
                    case 'text':
                    case 'textarea':
                    case 'select-one':
                    case 'select':
                    case 'select-multiple':
                    case 'date':
                    case 'number':
                    case 'tel':
                    case 'email':
                        a(this).val('');
                        break;
                    case 'checkbox':
                    case 'radio':
                        this.checked = false;
                        break;
                }
            });
        }, 
        
        this.toggleElements = function(element, action, timeout) {
            var that = this;
            0!=that.debug() && console.group('toggleElements');
            0!=that.debug() && console.log('toggleElements('+element+','+action+','+timeout+')');
            0!=that.debug() && console.log(new Error().stack);

            switch (timeout = void 0 !== timeout ? timeout : 0, action) {
                case "hide":
                    a(element).hide(timeout);
                    setTimeout(function() {
                        a(element).addClass('d-none');
                    }, timeout);
                    break;
                default:
                case "show":
                    a(element).removeClass('d-none').show(timeout);
                    break;
                case "slideup":
                    a(element).slideUp(timeout);
                    setTimeout(function() {
                        a(element).addClass('d-none');
                    }, timeout);
                    break;
                case "slidedown":
                    a(element).removeClass('d-none').slideDown(timeout);
                    break;
                case "fadeIn":
                    a(element).removeClass('d-none').fadeIn(timeout);
                    break;
                case "fadeOut":
                    a(element).fadeOut(timeout);
                    setTimeout(function() {
                        a(element).addClass('d-none');
                    }, timeout);
                    break;
            }
            
            0!=that.debug() && console.groupEnd('toggleElements');
        },
        
        this.verifyTerms = function(action,url) {
            /**
             * action = [load|view]
             * url = goto if disagree
             * $environment = determine [DEV|TEST|UAT|PROD] **PROD = empty/false
             * 
             **/
            var that = this,
                returnThis = false,
                vhagreedVersion = 'v1.00';
            if(url==='' || typeof url =='undefined'){
                url = DAWizard.config.url;
            }

            if($environment=='' || typeof $environment =='undefined' && ($environment!='dev' && $environment!='test' && $environment!='uat')){
                $environment = '';
            }
            
            0!=that.debug()&&(console.log('%cdebug enabled for $environment: '+$environment, consoleStylesWarning));
            // verify terms already agreed to via cookies?
            
            if(getUrlVars()["cookie"] && getUrlVars()["cookie"]==='false') {
                0!=that.debug()&&(console.warn('Reset cookie, done.'));
                if (window.sessionStorage) {
                    sessionStorage.setItem('vhagreed', '0');
                }
            }
            
            if (window.sessionStorage && sessionStorage.getItem("vhagreed")===vhagreedVersion) {
                // already agreed to for this session, show the page.
                0!=that.debug()&&(console.info('Terms previously accepted: '+vhagreedVersion));
                switch(action){
                    case 'view':
                        0!=that.debug()&&(console.group('Initiating Exempt Dev types...'));
                        $('div[id^="faq"]').parent().removeClass('hidden');
                        0!=that.debug()&&(console.log('initiated'));
                        0!=that.debug()&&(console.groupEnd());
                        returnThis = true;
                        break;
                    case 'load':
                        0!=that.debug()&&(console.group('Initiating DA WIzard...'));
                        
                        try {
                            if(that.init()){
                                0!=that.debug()&&(console.log('initiated'));
                                that.toggleElements('body .spinme', 'hide', 0);
                                that.toggleElements('.btn.btn-back-link', 'hide',0);
                                0!=that.debug() && console.log("hidden at 762");
                            } else {
                                throw "failed to initialise Wizard [e0x400.435]";
                            }
                        } catch(err) {
                            0!=that.debug()&&(console.error('failed: '+err));
                        }
                        
                        0!=that.debug()&&(console.groupEnd());
                        break;
                }
            }else{
                
                // get terms content
                0!=that.debug()&&(console.info('Terms not yet accepted. Current terms version: '+vhagreedVersion));
                $LayerJSON = {};
            	a.ajax({
            		type: "GET",
            		data: {"SQ_ENVIRONMENT":$environment},
            		url: DAWizard.config.urlTerms,
            		dataType: "text",
            		success: function(data) {
            			$LayerJSON['vhagreed'] = data;
            			0!=that.debug()&&(console.table($LayerJSON));
                        a.confirm({
                			title: 'Terms of Use ' + $environment.toUpperCase(),
                			icon: 'fas fa-exclamation-triangle',
                			theme: 'bootstrap', // bootstrap , supervan, material
                			content: $LayerJSON['vhagreed'],
                			animation: 'none',
                			animationClose: 'top',
                			columnClass: 'col-12 col-md-8 col-lg-8 col-lg-offset-2 col-md-offset-2 col-offset-0',
                			containerFluid: true, // this will add 'container-fluid' instead of 'container'
                			backgroundDismiss: function(){
                                return 'itIsNotconfirmed'; // force rejection of terms
                            },
                            onOpen: function () {
                                // after the modal is displayed.
                                a('body .jconfirm button.btn svg.svg-inline--fa').attr('role','presentation');
                            },
                			buttons: {
                                itIsconfirmed: {
                                    text: 'I agree',
                                    btnClass: 'btn-primary btn-submit',
                                    action: function(){
                                        if (window.sessionStorage) {
                                            sessionStorage.setItem('vhagreed', vhagreedVersion);
                                        }
                                        0!=that.debug()&&(console.info('Terms have been accepted: '+vhagreedVersion));
                                        switch(action){
                                            case 'view':
                                                a('div[id^="faq"]').parent().removeClass('d-none');
                                                break;
                                            case 'load':
                                                0!=that.debug()&&(console.group('Initiating DA WIzard...'));
                		                        try {
                                                    if(that.init()){
                                                        0!=that.debug()&&(console.log('initiated'));
                                                        that.toggleElements('.btn.btn-back-link', 'hide',0);
                                                        that.toggleElements('body .spinme', 'hide', 0);
                                                    } else {
                                                        throw "failed to initialise Wizard [e0x400.435]";
                                                    }
                                                } catch(err) {
                                                    0!=that.debug()&&(console.error('failed: '+err));
                                                }
                                                
                                                0!=that.debug()&&(console.groupEnd());
                                                break;
                                        }
                                    }
                                },
                				itIsNotconfirmed: {
                					text: 'I do not agree',
                					btnClass: 'btn-secondary btn-reset',
                					action: function () {
                						// user did not agreed to terms
                						// remove VH from DOM
                						try {
                						    if(action=='load'){
                						        a('#dawizard .wizard-answers').remove();
                						        a('#dawizard .wizard-questions').html('');
                						    }
                						    a('#dawizard .spinme .loading-message').text('Redirecting...');
                						    a('#dawizard .wizard-questions').html('<div id="vh-redirect" class="alert alert-warning active">Development approval wizard not loaded due to rejection of <a href="'+window.location.href+'">the terms of use</a>. Redirecting in <span data-count="10">10</span> seconds.</div>').removeClass('d-none');
                						    0!=that.debug()&&(console.warn('Terms rejected for: ' + vhagreedVersion + '\nWith debug disabled we would redirect the user to: ' + url +'\n\tVH removed'));
                						    
                                            $('html, body').animate({
                                                scrollTop: a("#vh-redirect").offset().top
                                            }, 800);
                						    
                						    function setRejection(){
                						        $myInterval = setInterval(function(){
                						            that.myTimer(url);
                						        }, 1000);
                						    }
                						    try {
                						        setRejection();
                						        0!=that.debug()&&(console.group('Redirection timer initiated'));
                						    } catch(err) {
                						        0!=that.debug()&&(console.error('Unable to start redirect timer: '+err));
                						    }
                						    
                						} catch(err) {
                						    0!=that.debug()&&(console.error('Terms rejected for: ' + vhagreedVersion + '\nUnable to remove VH from DOM:'+err));
                						}
                						if (window.sessionStorage) {
                						    sessionStorage.removeItem('vhagreed');
                						}
                						
                					}
                				}
                			}
                		});
                		
            		},
            		error:function(jqXHR, textStatus, errorThrown){
            			console.error( "error retrieving CSV file\n"+textStatus+": "+ errorThrown );
            			a.confirm({
            				title: 'Terms of use lookup error: '+errorThrown,
            				content: '<p>'+textStatus+' '+jqXHR['status']+' - e0x400.940</p><p>Please try again or if this error continues, <a target="_blank" title="Contact DPTI" href="' + sapp.defaults.url + '' + sapp.defaults.contactPage + '">contact us quoting the error.</a></p>',
            				animation: 'none',
            				theme: 'supervan',
            				animationClose: 'top',
            				columnClass: 'col-12 col-md-8 col-lg-6 col-lg-offset-3 col-md-offset-2 col-offset-0',
            				containerFluid: true, // this will add 'container-fluid' instead of 'container'
            				backgroundDismiss: function(){
                                return 'cancelAction';
                            },
                            onOpen: function () {
                                // after the modal is displayed.
                                a('body .jconfirm button.btn svg.svg-inline--fa').attr('role','presentation');
                            },
            				buttons: {
            					cancelAction: {
            						text: 'Close',
            						action: function () {
            							// close
            							location.reload();
            						}
            					}
            				}
            			});
            		}
            	});
            }
            return returnThis;
        },
        
        this.myTimer = function(url) {
            var that = this;
            var total = a('#dawizard .alert span[data-count]').text() - 1;
            a('#dawizard .alert span[data-count]').text(total);
            0!=that.debug()&&(console.log(total+1));
            if(total<=0){
                clearInterval($myInterval);
                0!=that.debug()&&(console.log('redirection inactive due to debug = true'));
                0!=that.debug()&&(console.groupEnd());
                if(that.debug()==false){
                    window.location.href = url;
                } else {
                    a('#dawizard .alert:has(span[data-count])').append('<div class="alert active alert-danger">Debug enabled, redirection will not occur.</div>');
                }
            }
        },
        
        this.initDAWizard = function() {
            // trying to gaffa tape the old DA WIzard to new world with different functions not yet fully defined, FML!
            var that = this;
            that.toggleElements('body .spinme', 'show', 0);
            0!=that.debug()&&(console.log('initDAWizard'));
            $currentPolicy = '';
            var defaultWizard = ''
                +'<div class="row no-gutters" id="ih-development">'
                +'  <div id="map-address-details">'
			    +'      <div class="col-12" id="address-details"></div>'
		        +'  </div>'
		        +'  <div class="col-12">'
    		    +'      <form name="ih-address" id="ih-address" action="POST" method="">'
    			+'      <div class="form-group has-feedback no-gutter">'
    			+'          <label class="control-label" for="address-lookup">Enter the property address</label>'
    			+'          <input type="hidden" name="address-lookup-location-x" id="address-lookup-location-x" value="" />'
    			+'          <input type="hidden" name="address-lookup-location-y" id="address-lookup-location-y" value="" />'
    			+'          <div class="input-group">'
    			+'              <span id="address-found"></span>'
    			+'              <input class="form-control input-lg" type="text" name="address-lookup" id="address-lookup" value="" placeholder="Search for property location" />'
    			+'              <div class="input-group-btn fetching">'
    			+'                  <i class="loader-xs d-none" style="font-size:24px;position: absolute;right: 55px;z-index: 9;top: 9px;"></i>'
    			+'                  <button class="btn btn-primary" style="right:0; position:absolute;line-height:1; width:45px; height: 38px;" data-action="clear" type="submit" title="Clear Search"><b title="clear search" aria-hidden="true">X</b><span class="sr-only">clear search</span></button>'
    			+'              </div>'
    			+'          </div>'
    			+'      </div>'
    			+'      <div class="form-group">'
    			+'          <a class="p-0 btn btn-link" data-link-action="daw-contactcouncil" href="/">I can\'t find my address in the list</a>'
    			+'      </div>'
    		    +'      </form>'
    	        +'  </div>'
    	        +'  <div clas="col-12" id="ih-zone">'
		        +'  </div>'
	            +'</div>';
            a('body #dawizard #ih').html(defaultWizard);
            if(DAWizardData==false) {
                a("body #ih-address .input-group i.loader-xs.d-none").removeClass('d-none');
                a('body #address-lookup').attr('disabled','disabled').attr('placeholder','...loading address database');
            }
            a('body').on('keyup keypress','#dawizard #ih-address', function(e) {
                var keyCode = e.keyCode || e.which;
                if (keyCode === 13) { 
                    e.preventDefault();
                    return false;
                }
            });
            // address lookup
            0!=that.debug() && console.log('new heading[4]: ' + a('body .da-answer#daw-wizard div[data-title-attr]').data('title-attr'));
            a('body .page-heading .h1').text(a('body .da-answer#daw-wizard div[data-title-attr]').data('title-attr'));
			var options = {
                url: function(phrase) {
                    a("body #ih-address .input-group i.loader-xs.d-none").removeClass('d-none');
                    
                    return "//lsa1.geohub.sa.gov.au/server/rest/services/Locators/SAGAF_Valuation/GeocodeServer/findAddressCandidates?Street=&Locality=&State=&Postcode=&category=&outFields=&maxLocations=12&searchExtent=&location=&distance=&magicKey=&f=pjson&outSR=3857&Single+Line+Input="+phrase+"";
                },
                categories: [
                    { 
                        listLocation: "candidates",
                        maxNumberOfElements: 15,
                        match: {
                			enabled: false
                		}
                }],
                
                getValue: function(element) {
					return element.address;
                },
                list: {
                    maxNumberOfElements: 12,
                    match: {
                        enabled: true
                    },
                    sort: {
                        enabled: true
                    },
                    onLoadEvent: function() {
                        a("body #ih-address .input-group i.loader-xs").removeClass('d-none');
                    },
                    onSelectItemEvent: function() {
                        a("body #ih-address .input-group i.loader-xs").removeClass('d-none');
                    },
                    onShowListEvent: function() {
                        a("body #ih-address .input-group i.loader-xs").addClass('d-none');
                    },
                    onHideListEvent: function() {
                        a("body #ih-address .input-group i.loader-xs").addClass('d-none');
                    },
                    onChooseEvent: function() {
                        $exitNow = false;
						a("body #ih-address .input-group i.loader-xs").addClass('d-none');
						a("body #ih-address .daw-continue").remove();
            		    var selectedItemValue = a("body #address-lookup").val();
						var searchLocation = a("body #address-lookup").getSelectedItemData().location;
						a('body #address-lookup-location-x').val(searchLocation['x']);
				        a('body #address-lookup-location-y').val(searchLocation['y']);
				        $LatLon = that.convertCoordinates(a('body #address-lookup-location-x').val(), a('body #address-lookup-location-y').val());
                        //var latlongIs = $LatLon.Lat+","+$LatLon.Lon;
						var latlongIs = a('body #address-lookup-location-x').val()+','+a('body #address-lookup-location-y').val();
            		    0!=that.debug() && console.log('LatLon: '+latlongIs+'\nsearchLocation: '+JSON.stringify(searchLocation,null,2));
            		    if(selectedItemValue==='' || !selectedItemValue){
            		        $LatLon = that.convertCoordinates('15126839.396204999', '-3732127.384919313');
            		        a('body #property-details').addClass('d-none');
            		    }else{
							$removeValFromZones.length = 0;
							
							// show the 'on continue' button
							a.when(
							    a('body #ih-address a[data-link-action]').hide().after(DAWizard.config.continueButton)
							).then(
							    
							    setTimeout(function(){
                                    a('body #ih-address .daw-continue button[type="submit"]').attr('data-final-step','true').focus().find('svg.svg-inline--fa').attr('role','presentation')
                                },200)
							);
							
							a('body').on('click','#ih-address .daw-continue button[type="submit"]', function(){
							    a('body .spinme .loading-message').text('calculating results...');
							    that.toggleElements('body .spinme', 'show', 0);
							    a.when( that.getPropertyDetails(selectedItemValue) ).done(function(x) {
    								a('body #ih-address .daw-continue button[type="submit"]').remove();
    								a('body #property-details.d-none').removeClass('d-none');
    								a('body #address-verify.d-none').removeClass('d-none');
    								a('body #ih #address-details').remove();
    								a('body #ih .daw-restart').remove();
    								a('body #ih-zone').after(DAWizard.config.restartButton);
    								
                                });
							});
            		    }
            		    
            		}
                },
                placeholder: "",
                template: {
        		type: "custom",
            		method: function(value, item) {
            			return value;
            		}
        	    },
                ajaxSettings: {
                    dataType: "json",
                    method: "GET",
                    data: {
                      dataType: "json"
                    }
                }
            };
            
            a("body #address-lookup").easyAutocomplete(options);
            a('body .easy-autocomplete').css("width", "100%");
        },
        // 
        this.loadPoliciesEtc = function(thisModalIs){
			var that = this;
// 			var noHashID = thisModalIs.substring(1, thisModalIs.length);
			0!=that.debug() && console.log('loadPoliciesEtc: [thisModalIs=' + thisModalIs + ']');
			$exclusionZones.length = 0;
			$exclusionPolicies.length = 0;
// 			0!=that.debug() && a('body #debug_options').append("<br>$devTypes['dev-type']: <pre>" + JSON.stringify($devTypes['dev-type'], null, 2) + "</pre>");
			a.each( $devTypes['dev-type'], function( key, value ) {
				if (key === thisModalIs){
					a.each( value, function(key, value){
						// set id: used for dev type filtering logic
						if(key==='id'){
							$selectedDevType = value;
						}
						// set title: modal-title
						if(key==='title'){
							0!=that.debug() && a('body #debug_options').append("<br>set modaltitle: " + value);
						}
						// zones: Object
						if(key==='zones'){
							a.each( value, function(key, zvalue){
								if(zvalue!=''){$exclusionZones.push(zvalue);}
							});
						}
						// policies: Object
						if(key==='policies'){
							a.each( value, function(key, pvalue){
								if(pvalue!=''){$exclusionPolicies.push(pvalue);}
							});
						}
						
						// exemption: Object
						if(key==='exemption'){
							a.each( value, function(key, tvalue){
								switch (key) {
									case 'list':
										var listItems = "";

										for (var a in tvalue)
										{
											listItems += "<li class='list-group-item in-active'>"+tvalue[a]+"</li>";
										}
								        // a('ul[data-role="exemption-zones"]').html(listItems);
								        0!=that.debug() && a('body #debug_options').append("<ul><li>exemption: list</li>" + listItems + "</ul>");
										break;
									case 'footer':
								        // a('#exemptions #ih-modal-footer').html(tvalue);
								        0!=that.debug() && a('body #debug_options').append("<br>exemption footer: " + tvalue);
										break;
								}
							});
						}
						
						// policy: Object
						if(key==='policy'){
                            //$('#policy').html(value);
							//console.log(value);
							$currentPolicy = '';
							$currentPolicy += '<div class="tab-pane" id="policy">';
							$currentPolicy += 			value;
							$currentPolicy += 		'</div>';
				// 			0!=that.debug() && a('body #debug_options').append("<br>$currentPolicy: <pr>" + $currentPolicy + "</pre>");
						}
					});
				}
			});
		},
        
        //var csv is the CSV file with headers
        this.csvJSON = function(csv){
            var lines=csv.split(/\r\n|\n/);
            var result = [];
            var headers=lines[0].split(",");
            for(var i=1;i<lines.length;i++){
                var obj = {};
                var currentline=lines[i].split(",");
                for(var j=0;j<headers.length;j++){
                    obj[headers[j]] = currentline[j];
                }
                result.push(obj);
            }
            return result; //JSON
        },
        
        this.processCSVdata = function(allText) {
			var that = this;
			var allTextLines = allText.split(/\r\n|\n/);
			var headers = allTextLines[0].split(',');
			var lines = [];

			for (var i=1; i<allTextLines.length; i++) {
				var data = allTextLines[i].split(',');
				if (data.length == headers.length) {

					var tarr = [];
					for (var j=0; j<headers.length; j++) {
						/*tarr.push(headers[j]+":"+data[j]);*/
						tarr.push(data[j]);
					}
					lines.push(tarr);
				}
			}
			return lines;
		},
        
        this.convertCoordinates = function (mercX, mercY,arrit){
		    if (arrit === undefined || !arrit || arrit=== null) {arrit = "json";}
		    var rMajor = 6378137; //Equatorial Radius, WGS84
            var shift  = Math.PI * rMajor;
            var lon    = mercX / shift * 180.0;
            var lat    = mercY / shift * 180.0;
            lat = 180 / Math.PI * (2 * Math.atan(Math.exp(lat * Math.PI / 180.0)) - Math.PI / 2.0);
            switch(arrit){
                default:
                case 'json':
                    return { 'Lon': lon, 'Lat': lat };
                    break;
                case 'arr':
                    var thisarr = [lat,lon];
                    return thisarr;
                    break;
            }  
		},
		
		this.getPropertyDetails = function(address,actType){
			var that = this;
			var ValuationNumber = false;
			if("undefined"==typeof actType || ""==actType){
			    actType="dev";
			    0!=that.debug() && console.log("actType was undefined: defaults to [dev]");
			}
			
			a("body #ih-address .input-group i.loader-xs").addClass('d-none');
			that.toggleElements('body .spinme', 'show', 0);
			var loc = [];
			loc["x"] = a('#address-lookup-location-x').val();
			loc["y"] = a('#address-lookup-location-y').val();
			var ts = Math.round((new Date()).getTime() / 1000);
			$LatLon = that.convertCoordinates(loc["x"], loc["y"]);
			0!=that.debug() && console.log("$LatLon:" + JSON.stringify($LatLon, null, 2));
			// get valuation number
		    a.ajax({
				type:'GET',
				// url: '//services.geohub.sa.gov.au/v1/intersects/valuationaddress?location={"x":'+loc["x"]+' ,"y":'+loc["y"]+' ,"spatialReference":{"wkid":3857}}', 
				url: '//services.geohub.sa.gov.au/v1/intersects/valuationaddress?location=%7B%22x%22%3A'+loc["x"]+'%20%2C%22y%22%3A'+loc["y"]+'%20%2C%22spatialReference%22%3A%7B%22wkid%22%3A3857%7D%7D', 
				cache: false,
				crossDomain: true,
				dataType: "json",
				jsonp: false,
				headers: {
				    "x-api-key":"BdD9YzUz9X5p1v1eK9ouA3WqmxMMyruz4JGQWaQd"
				}
			})
			.done(function(data){
			    data = JSON.stringify(data, null, 2);
				data = jQuery.parseJSON(data);
				ValuationNumber = data.ValuationAddress[0]['valuation_no'];
				
			    0!=that.debug() && console.log(JSON.stringify(data, null, 2));
			    if('undefined' != typeof ValuationNumber && false != ValuationNumber){
    			    that.getPDI(ValuationNumber,loc);
    			} else {
    			    that.getDEV(loc);
    			}
			})
			.fail(function(jqXHR, textStatus, errorThrown){
				ValuationNumber = false;
				var $errorcode = '502 (invalid data found)';
				0!=that.debug() && console.log($errorcode+" : "+jqXHR.getAllResponseHeaders());
				0!=that.debug() && console.log(JSON.stringify(jqXHR, null, 2));
				a.confirm({
					title: 'Error: '+$errorcode,
					content: '<p>'+jqXHR.statusText+'</p><p>Please try again or if this error continues, <a target="_blank" title="Contact DPTI" href="https://www.saplanningportal.sa.gov.au/contact_us">contact us quoting the error.</a></p><p>If multiple addresses are available to select from, please try another from the list as we may have detected multiple valuations for the same parcel.</p>',
					animation: 'none',
					theme: 'supervan',
					animationClose: 'top',
					columnClass: 'col-10 col-md-8 col-lg-6 col-lg-offset-3 col-md-offset-2 col-offset-1',
					containerFluid: false, // this will add 'container-fluid' instead of 'container'
					backgroundDismiss: false, // this will just close the modal
					buttons: {
						cancelAction: {
							text: 'Close',
							action: function () {
								// close
							}
						}
					}
				});
			});
			
		},
		
		this.getPDI = function(ValuationNumber,loc){
		    var that = this;
		    0!=that.debug() && console.log('Get PDI: '+ValuationNumber);
		    // https://services.geohub.sa.gov.au/v1/intersects/zonesandoverlays
		    a.ajax({
				type:'GET',
				url: 'https://services.geohub.sa.gov.au/v1/intersects/zonesandoverlays?pid=' + ValuationNumber,
				cache: false,
				crossDomain: true,
				dataType: "json",
				headers: {
				    "x-api-key":"BdD9YzUz9X5p1v1eK9ouA3WqmxMMyruz4JGQWaQd"
				}
			})
			.done(function(data){
				data = JSON.stringify(data, null, 2);
				data = jQuery.parseJSON(data);
			    0!=that.debug() && console.log(JSON.stringify(data, null, 2));
				if('error' in data){
				    var $errorcode = 'e0x500.548';
					0!=that.debug() && console.log("nothing happened: (data) - "+JSON.stringify(data['error'], null, 2));
					a.confirm({
						title: 'Error '+data['error']+': '+$errorcode,
						content: '<p>'+data['error']+'</p><p>Please try again or if this error continues, <a target="_blank" title="Contact DPTI" href="https://www.saplanningportal.sa.gov.au/contact_us">contact us quoting the error.</a></p>',
						animation: 'none',
						theme: 'supervan',
						animationClose: 'top',
						columnClass: 'col-10 col-md-8 col-lg-6 col-lg-offset-3 col-md-offset-2 col-offset-1',
						containerFluid: false, // this will add 'container-fluid' instead of 'container'
			// 			backgroundDismiss: false, // this will just close the modal
						backgroundDismiss: function(){
                            return 'cancelAction'; // force rejection of terms
                        },
						buttons: {
							cancelAction: {
								text: 'Close',
								action: function () {
									// close
								}
							}
						}
					});
				} else if( typeof data.zoningoverlays.zone == "undefined"  
                        || data.zoningoverlays.zone == null  
                        || data.zoningoverlays.zone.length == null  
                        || data.zoningoverlays.zone.length < 1 ){
				    try {
					   // var loc = [];
        //     			loc["x"] = a('#address-lookup-location-x').val();
        //     			loc["y"] = a('#address-lookup-location-y').val();
					    that.getDEV(loc);
					} catch(err) {
					    console.log(err);
					}
				} else {
				    
					try {
					    a.when( that.sortPropertyDetailsPDIAct(data) ).done(function(x) {
					        a("#ih #exemptions #ih-zone").html('');
					        that.toggleElements('body .spinme', 'hide', 0);
					        a('body .spinme .loading-message').text(DAWizard.messages.loadingMessage);
					        0!=that.debug() && console.log("hidden at 1321");
					    });
					} catch(err) {
					    console.log(err);
					}
				}
			})
			.fail(function(jqXHR, textStatus, errorThrown){
			    a.confirm({
					title: 'Error: 1287.502',
					content: '<p>'+jqXHR.statusText+'</p><p>Please try again or if this error continues, <a target="_blank" title="Contact DPTI" href="https://www.saplanningportal.sa.gov.au/contact_us">contact us quoting the error.</a></p><p>If multiple addresses are available to select from, please try another from the list as we may have detected multiple valuations for the same parcel.</p>',
					animation: 'none',
					theme: 'supervan',
					animationClose: 'top',
					columnClass: 'col-10 col-md-8 col-lg-6 col-lg-offset-3 col-md-offset-2 col-offset-1',
					containerFluid: false, // this will add 'container-fluid' instead of 'container'
					backgroundDismiss: false, // this will just close the modal
					buttons: {
						cancelAction: {
							text: 'Close',
							action: function () {
								// close
							}
						}
					}
				});
			});
		},
		
		this.getDEV = function(loc){
		    var that = this;
		    0!=that.debug() && console.log('Get DEV: '+JSON.stringify(loc, null, 2));
		    a.ajax({
				type:'GET',
				url: '//services.geohub.sa.gov.au/v1/intersects/parcel/developmentplan?location=%7B%22x%22%3A'+loc["x"]+'%20%2C%22y%22%3A'+loc["y"]+'%20%2C%22spatialReference%22%3A%7B%22wkid%22%3A3857%7D%7D&returnGeometry=True',
				cache: false,
				crossDomain: true,
				dataType: "json",
				headers: {
				    "x-api-key":"BdD9YzUz9X5p1v1eK9ouA3WqmxMMyruz4JGQWaQd"
				}
			})
			.done(function(data){
				data = JSON.stringify(data, null, 2);
				data = jQuery.parseJSON(data);
			    0!=that.debug() && console.log(JSON.stringify(data, null, 2));
				0!=that.debug() && console.log("x:"+loc["x"]+"\ny:"+loc["y"]);
				if('error' in data){
				    var $errorcode = 'e0x500.548';
					0!=that.debug() && console.log("nothing happened: (data) - "+JSON.stringify(data['error'], null, 2));
					a.confirm({
						title: 'Error '+data['error']+': '+$errorcode,
						content: '<p>'+data['error']+'</p><p>Please try again or if this error continues, <a target="_blank" title="Contact DPTI" href="https://www.saplanningportal.sa.gov.au/contact_us">contact us quoting the error.</a></p>',
						animation: 'none',
						theme: 'supervan',
						animationClose: 'top',
						columnClass: 'col-10 col-md-8 col-lg-6 col-lg-offset-3 col-md-offset-2 col-offset-1',
						containerFluid: false, // this will add 'container-fluid' instead of 'container'
			// 			backgroundDismiss: false, // this will just close the modal
						backgroundDismiss: function(){
                            return 'cancelAction'; // force rejection of terms
                        },
						buttons: {
							cancelAction: {
								text: 'Close',
								action: function () {
									// close
								}
							}
						}
					});
				}else{
				    
					0!=that.debug() && console.log(JSON.stringify(data, null, 2));
					try {
					    a.when( that.sortPropertyDetailsDevAct(data) ).done(function(x) {
					        a("#ih #exemptions #ih-zone").html('');
					        that.toggleElements('body .spinme', 'hide', 0);
					        a('body .spinme .loading-message').text(DAWizard.messages.loadingMessage);
					        0!=that.debug() && console.log("hidden at 1399");
					    });
					} catch(err) {
					    console.log(err);
					}
				}
			})
			.fail(function(jqXHR, textStatus, errorThrown){
			    a.confirm({
					title: 'Error: 1366.502',
					content: '<p>'+jqXHR.statusText+'</p><p>Please try again or if this error continues, <a target="_blank" title="Contact DPTI" href="https://www.saplanningportal.sa.gov.au/contact_us">contact us quoting the error.</a></p><p>If multiple addresses are available to select from, please try another from the list as we may have detected multiple valuations for the same parcel.</p>',
					animation: 'none',
					theme: 'supervan',
					animationClose: 'top',
					columnClass: 'col-10 col-md-8 col-lg-6 col-lg-offset-3 col-md-offset-2 col-offset-1',
					containerFluid: false, // this will add 'container-fluid' instead of 'container'
					backgroundDismiss: false, // this will just close the modal
					buttons: {
						cancelAction: {
							text: 'Close',
							action: function () {
								// close
							}
						}
					}
				});
			});
		},
		
		this.sortPropertyDetailsPDIAct = function(element) {
			var that = this;
			
			$LayerJSON['HeritageClassOne59'] = '';
			$LayerJSON['HeritageClassTwo59'] = '';
			$LayerJSON['Zone23'] = [];
			$LayerJSON['ZoneMeaning23'] = [];
			$LayerJSON['Policy23'] = [];
			$LayerJSON['PolicyMeaning23'] = [];
			var cnt=0;
			
			a.each( element['zoningoverlays']['overlay'], function( key, value ) {
				0!=that.debug() && console.log('zoningoverlays - overlay: '+JSON.stringify(value, null, 2));
				a.each(value, function (k,v) {

					if(k==='id') {cnt++;$LayerJSON['Zone23'][cnt] = v;}
					if(k==='descr') {cnt++;$LayerJSON['ZoneMeaning23'][cnt] = v;}
				});
			});
			a.each( element['zoningoverlays']['zone'], function( key, value ) {
				0!=that.debug() && console.log('zoningoverlays - zone: '+JSON.stringify(value, null, 2));
				a.each(value, function (k,v) {
					
					if(k==='id') {cnt++;$LayerJSON['Policy23'][cnt] = v;}
					if(k==='name') {cnt++;$LayerJSON['PolicyMeaning23'][cnt] = v;}
				});
			});
			a.each( element['zoningoverlays']['subzone'], function( key, value ) {
				0!=that.debug() && console.log('zoningoverlays - subzone: '+JSON.stringify(value, null, 2));
				a.each(value, function (k,v) {
					
					if(k==='id') Precinct23 = v;
					if(k==='name') PrecinctMeaning23 = v;
				});
			});
			$LayerJSON['Zone23'] = that.removeDuplicates($LayerJSON['Zone23']);
			$LayerJSON['ZoneMeaning23'] = that.removeDuplicates($LayerJSON['ZoneMeaning23']);
			$LayerJSON['Policy23'] = that.removeDuplicates($LayerJSON['Policy23']);
			$LayerJSON['PolicyMeaning23'] = that.removeDuplicates($LayerJSON['PolicyMeaning23']);
			
// 			console.log( JSON.stringify(element,null,2) );
			if('error' in element){
				$GISerror = (element['error']!="") ? element['error'] : '';
			}
            $LayerJSON['riskCategory22'] = 'Excluded';
			
			$LayerJSON['locationX'] = element['location']['x'];
			$LayerJSON['locationY'] = element['location']['y'];
			/*console.log("locationX: "+locationX+"\nlocationY: "+locationY);*/

			if($GISerror!=''){
				a('#ih #exemptions #ih-zone').html('<div class="alert alert-error"><p>'+element['error']+'</p>We could not fetch all the details for the address provided. Please check the address and try again.</div>');
			}else{	
				var selectedItemValue = $("#address-lookup").val();
				0!=that.debug() && console.log('theDevType: ' + theDevType);
				// that.loadPoliciesEtc(theDevType);
				a.when( that.loadPoliciesEtc(theDevType) )
				    .then(function(){
				        that.matchZones($LayerJSON['Zone23'],$LayerJSON['Policy23'],'pdi');
				    })
				    .done(function(x) {
    				    // that.toggleElements('body .spinme', 'hide', 200);
    				    0!=that.debug() && console.log('new heading[5]: ' + theCurrentStepLabel);
    				    a('body .page-heading .h1').text(theCurrentStepLabel);
    				    var theAddress = a('body #address-lookup').val();
    				    
    				    a('body span#address-found').html('<p><i class="fas fa-map-marker-alt" aria-hidden="true"></i> <span class="pl-2">' + theAddress + '</span></p>');
    				    
                	    return true;
				        
				    });
			}
		},
		
		this.sortPropertyDetailsDevAct = function(element) {
			var that = this;
			var PolyLinesBoundarys,PolyLinesBoundary;
			$LayerJSON['HeritageClassOne59'] = '';
			$LayerJSON['HeritageClassTwo59'] = '';
			$LayerJSON['Zone23'] = [];
			$LayerJSON['ZoneMeaning23'] = [];
			$LayerJSON['Policy23'] = [];
			$LayerJSON['PolicyMeaning23'] = [];
			var cnt=0;
			
			a.each( element['DevPlanZoning'], function( key, value ) {
				
				a.each(value, function (k,v) {
					if(k==='devplan_code') $LayerJSON['LGA17'] = v;
					if(k==='zone') {cnt++;$LayerJSON['Zone23'][cnt] = v;}
					if(k==='zone_meaning') {cnt++;$LayerJSON['ZoneMeaning23'][cnt] = v;}
					if(k==='precinct') Precinct23 = v;
					if(k==='precinct_meaning') PrecinctMeaning23 = v;
					if(k==='policy') {cnt++;$LayerJSON['Policy23'][cnt] = v;}
					if(k==='policy_meaning') {cnt++;$LayerJSON['PolicyMeaning23'][cnt] = v;}
					0!=that.debug() && console.log('key: '+k+'\nvalue: '+value);
				});
			});
			$LayerJSON['Zone23'] = that.removeDuplicates($LayerJSON['Zone23']);
			$LayerJSON['ZoneMeaning23'] = that.removeDuplicates($LayerJSON['ZoneMeaning23']);
			$LayerJSON['Policy23'] = that.removeDuplicates($LayerJSON['Policy23']);
			$LayerJSON['PolicyMeaning23'] = that.removeDuplicates($LayerJSON['PolicyMeaning23']);
			
// 			console.log( JSON.stringify(element,null,2) );
			if('error' in element){
				$GISerror = (element['error']!="") ? element['error'] : '';
			}
			if('HeritagePlaces' in element){
				a.each( element['HeritagePlaces'], function( key, value ) {
					a.each(value, function (k,v) {
						var HeritageClassTwo59 = 'N/A';
						var StateHeritageDetails59 = 'N/A';
						if(k==='heritageclass1') $LayerJSON['HeritageClassOne59'] = v;
						if(k==='heritageclass2') $LayerJSON['HeritageClassTwo59'] = v;
					});
				});
			}
			
			switch($LayerJSON['HeritageClassOne59']){
				default:
				case 'N/A':
					$exitNow = false;
					break;
				case 'Local':
				case 'L':
					//$exclusionZones.push('Local');
					//$removeValFromZones.push('Local');
					$LayerJSON['HeritageClassOne59'] = "Local";
					break;
				case 'Contributory':
				case 'C':
					$LayerJSON['HeritageClassOne59'] = "";
					break;
				case 'State':
				case 'S':
					//$exclusionZones.push('State');
					//$removeValFromZones.push('State');
					$LayerJSON['HeritageClassOne59'] = "State";
					$exitNow = true;
					break;
			}
			
			switch($LayerJSON['HeritageClassTwo59']){
				default:
				case 'N/A':
					$exitNow = false;
					break;
				case 'Local':
				case 'L':
					//$exclusionZones.push('Local');
					//$removeValFromZones.push('Local');
					$LayerJSON['HeritageClassTwo59'] = "Local";
					break;
				case 'Contributory':
				case 'C':
					$LayerJSON['HeritageClassTwo59'] = "";
					break;
				case 'State':
				case 'S':
					//$exclusionZones.push('State');
					//$removeValFromZones.push('State');
					$LayerJSON['HeritageClassTwo59'] = "State";
					$exitNow = true;
					break;
			}
			//console.log($LayerJSON['HeritageClassOne59']);
			// high, medium, general, Excluded
			$LayerJSON['riskCategory22'] = 'Excluded';
			if('BushfireProtectionAreas' in element){
				
				a.each( element['BushfireProtectionAreas'], function( key, value ) {
					a.each(value, function (k,v) {
						if(k==='code') $LayerJSON['riskCategory22'] = v;
					});
				});
			}
			$LayerJSON['locationX'] = element['location']['x'];
			$LayerJSON['locationY'] = element['location']['y'];
			/*console.log("locationX: "+locationX+"\nlocationY: "+locationY);*/

			if($GISerror!=''){
				a('#ih #exemptions #ih-zone').html('<div class="alert alert-error"><p>'+element['error']+'</p>We could not fetch all the details for the address provided. Please check the address and try again.</div>');
			}else{	
				var selectedItemValue = $("#address-lookup").val();
				0!=that.debug() && console.log('theDevType: ' + theDevType);
				// that.loadPoliciesEtc(theDevType);
				a.when( that.loadPoliciesEtc(theDevType) )
				    .then(function(){
				        that.matchZones($LayerJSON['Zone23'],$LayerJSON['Policy23'],'dev');
				    })
				    .done(function(x) {
    				    // that.toggleElements('body .spinme', 'hide', 200);
    				    0!=that.debug() && console.log('new heading[5]: ' + theCurrentStepLabel);
    				    a('body .page-heading .h1').text(theCurrentStepLabel);
    				    var theAddress = a('body #address-lookup').val();
    				    
    				    a('body span#address-found').html('<p><i class="fas fa-map-marker-alt" aria-hidden="true"></i> <span class="pl-2">' + theAddress + '</span></p>');
    				    
                	    return true;
				        
				    });
			}
		},
		
		this.removeDuplicates = function (arr) {
			var unique_array = [];
			for(var i = 0;i < arr.length; i++){
				if(unique_array.indexOf(arr[i]) == -1 && arr[i]!= undefined){
					unique_array.push(arr[i]);
				}
			}
			return unique_array;
		},
		
		this.matchZones = function (zone,policy,theAct) {
			var that = this;
			0!=that.debug()&&(console.group('Matching Zones and policies'));
			$replaceThis = false, $flagHistoric = false, $localException = false;
			var statusMessage = '<ul>';
			var statusMessageCnt = 0;
			var cnt = 0;
			switch(theAct){
			    case 'dev':
			        var zoneOverlay = 'Zone';
			        break;
			    case 'pdi':
			        var zoneOverlay = 'Overlay'; 
			    break;
			}
			a.each( zone, function( key, value ) {
				if(a.inArray(value, $exclusionZones) !== -1){
					if($LayerJSON['Zone23'][cnt]===value){
					    statusMessage+= "<li>"+zoneOverlay+": "+$LayerJSON['ZoneMeaning23'][cnt]+"</li>";
						statusMessageCnt++;
						// check if dev type outbuilding and highlight historic wording in policy
                        0!=that.debug()&&(console.log(" match ("+value+"): "+$LayerJSON['ZoneMeaning23'][cnt]));
						// update to switch statement tzoneOverlayo allow for irregular zone checks
						switch ($selectedDevType) {
						    case 'outbuilding':
					        case 'solar-panels':
					            if(value!="HF" && value!="Fl" && value!="WC" && value!="RMFl" && value!="UW(Fl)" && value!="SHA(CLG)"){
        							$flagHistoric = true;
        							0!=that.debug()&&(console.log('sub zone match ('+value+'): '+$selectedDevType));
        						}else{
        						    $replaceThis = true;
        						    0!=that.debug()&&(console.log('401 no '+zoneOverlay+' match found'));
        						}
					            break;
						    case 'fences':
					            if(value==="RC" || value==="RS(BF)"){
        							$flagHistoric = true;
        							$highlightHidden = {"data-rel-zone":""+value+"","data-rel-policies":"null","data-rel-lga":""+$LayerJSON['LGA17']+""};
        							0!=that.debug()&&(console.log('sub zone match ('+value+'): '+$selectedDevType));
        						}else{
        						    $replaceThis = true;
        						    0!=that.debug()&&(console.log('411 no '+zoneOverlay+' match found'));
        						}
					            break;
					        default:
						        $replaceThis = true;
						        break;
						}
					}else{
						zone = '';
						0!=that.debug()&&(console.log('421 no '+zoneOverlay+' match found'));
					}	
				} else {
				    0!=that.debug()&&(console.log('423 no '+zoneOverlay+' match found'));
				}
				cnt++;
			});
			
			if (a.inArray($LayerJSON['HeritageClassOne59'], $exclusionZones) !== -1){
				zone = 'Heritage';
				statusMessage+= "<li>"+$LayerJSON['HeritageClassOne59']+" Heritage listed</li>";
				statusMessageCnt++;
				$replaceThis = true;
				0!=that.debug()&&(console.log("Heritage listed (class one): "+$LayerJSON['HeritageClassOne59']));
			}
			if (a.inArray($LayerJSON['HeritageClassTwo59'], $exclusionZones) !== -1){
				zone = 'Heritage';
				statusMessage+= "<li>"+$LayerJSON['HeritageClassTwo59']+" Heritage listed</li>";
				statusMessageCnt++;
				$replaceThis = true;
				0!=that.debug()&&(console.log("Heritage listed (class two): "+$LayerJSON['HeritageClassTwo59']));
			}
			
			var policyOutbuilding = "";
			var cnt = 0;
			a.each( policy, function( key, value ) {
                var valued = $LayerJSON['LGA17']+"%%"+value;
                
				if(a.inArray(valued, $exclusionPolicies) !== -1){
					if($LayerJSON['Policy23'][cnt]===value){
						if($selectedDevType==='outbuilding' && value != '13' && value != '19' && value != '2' && value != '7' && value != '8' && value != '5' && value != '6' && value != '15' && value != '18'){
							$flagHistoric = true;
							var policyOutbuilding = " ["+$LayerJSON['PolicyMeaning23'][cnt]+"]";
						}else{
							$replaceThis = true;
						}
						
						statusMessage+= "<li>Policy: ("+$LayerJSON['Policy23'][cnt]+") "+$LayerJSON['PolicyMeaning23'][cnt]+"";
						0!=that.debug()&&(console.log("Policy: ("+$LayerJSON['Policy23'][cnt]+") "+$LayerJSON['PolicyMeaning23'][cnt]));
						if(zone==''){
							statusMessage+= "<br>(<em>"+zoneOverlay+": "+$LayerJSON['ZoneMeaning23'][cnt]+"</em>)";
							0!=that.debug()&&(console.log(zoneOverlay+": "+$LayerJSON['ZoneMeaning23'][cnt]));
						}
						statusMessage+= "</li>";
						statusMessageCnt++;
						
					}
				}
				cnt++;
			});

			if (a.inArray($LayerJSON['HeritageClassOne59'], $exclusionPolicies) !== -1){
				policy = 'Heritage';
				// console.log($exclusionPolicies);
				statusMessage+= "<li>"+$LayerJSON['HeritageClassOne59']+" Heritage listed</li>";
				statusMessageCnt++;
				$replaceThis = true;
				0!=that.debug()&&(console.log("Heritage listed (class one): "+$LayerJSON['HeritageClassOne59']));
			}
			if (jQuery.inArray($LayerJSON['HeritageClassTwo59'], $exclusionPolicies) !== -1){
				policy = 'Heritage';
				statusMessage+= "<li>"+$LayerJSON['HeritageClassTwo59']+" Heritage listed</li>";
				statusMessageCnt++;
				$replaceThis = true;
				0!=that.debug()&&(console.log("Heritage listed (class two): "+$LayerJSON['HeritageClassTwo59']));
			}
			
			if(zone==='Heritage' && ($LayerJSON['HeritageClassOne59'] === 'Local' || $LayerJSON['HeritageClassTwo59'] === 'Local') && ($LayerJSON['HeritageClassOne59'] != 'State' && $LayerJSON['HeritageClassTwo59'] != 'State')){
			    $flagHistoric = true;
			    if($selectedDevType==='solar-panels'){
			        $replaceThis = false;
			        policyOutbuilding+= "Local Heritage place";
			        $localException = true;
			    }
			}
 			0!=that.debug() && console.log("Policy: "+policy);
			if($LayerJSON['riskCategory22']!='Excluded'){
				if($selectedDevType==='decks'){
					$replaceThis = true;
					statusMessage+= "<li>Bushfire Risk: "+$LayerJSON['riskCategory22']+"</li>";
					statusMessageCnt++;
					0!=that.debug()&&(console.log("Bushfire Risk: "+$LayerJSON['riskCategory22']));
				}
			}

			0!=that.debug() && console.log($exclusionPolicies);
			var tableOfContent = "<table class='table table-responsive' style='margin-bottom: 0px;margin-top: 10px;'><tr><th colspan=\"2\">Your property is located within:</th></tr><tr><td class=\"zone-policy-head\">Zone:</td><td class=\"zone-policy-body\">"+$LayerJSON['Zone23']+" ("+$LayerJSON['ZoneMeaning23']+")</td></tr>";
			if($LayerJSON['Policy23']!=="N/A"){
				tableOfContent+= "<tr><td class=\"zone-policy-head\">Policy:</td><td class=\"zone-policy-body\">"+$LayerJSON['Policy23']+" ("+$LayerJSON['PolicyMeaning23']+")</td></tr>";
			}

			tableOfContent+="</tr></table>";
			statusMessage+= "</div>";
			$LayerJSON['LGA17url'] = (function () { return; })();
 			0!=that.debug() && console.log(JSON.stringify($LayerJSON,null,2));
			if($LayerJSON['LGA17']!='N/A' && $LayerJSON['LGA17']!='' && $LayerJSON['LGA17']!=undefined){
				a.each($LayerJSON.councilCSV, function(i, v) {
					if (v.DEVPLAN_CODE == $LayerJSON['LGA17']) {
						$LayerJSON['LGA17url'] = "<a target=\"_blank\" href=\""+v.URL+"\">"+v.Name+"</a>";
						return;
					}
				});
			}else{
				$LayerJSON['LGA17'] = "";
				$LayerJSON['LGA17url'] = "<em>(please enter your address for council details)</em>";
			}
			if(statusMessageCnt>1){
				var areaPlural = "zones/areas";
			}else{
				var areaPlural = "a zone/area";
			}
			switch($selectedDevType){
			    case 'solar-panels':
			    case 'decks':
			    case 'spas':
			    case 'pergolas':
			    case 'shade-sail':
			    case 'trees':
			    case 'water-tanks':
			        var footerDis = "<div id=\"ih-modal-footer\">&nbsp;</div>";
			        break;
			    default:
			        var footerDis = "<div id=\"ih-modal-footer\">Disclaimer: Please check with your local council to see if your property resides within a watercourse or flood plain zone. Watercourse/Flood Prone Areas are specific to your Council - Not all application areas can be checked online.</div>";
			        break;
			}
			var LNWAC = ["LNWCA_CW","LNWCA_E","LNWCA_F","LNWCA_FN","LNWCA_MA","LNWCA_R","LNWCA_W","LNWCA"];
			// up to use council csv
			
            // PREFERED METHOD: IE doesn't like it though;
            // var lgaIndex = $LayerJSON.councilCSV.findIndex(x => x['DEVPLAN_CODE'] === $LayerJSON['LGA17']);
            function theLGA(lga) { 
                return lga['DEVPLAN_CODE'] === $LayerJSON['LGA17'];
            }
            var lgaIndex = a($LayerJSON.councilCSV).find(theLGA);
            
			
            // if(a.inArray( $LayerJSON['LGA17'], LNWAC ) >= 0){
            if(lgaIndex.Phase == 'One'){
                a.when( 
                    that.displayResults(
                        {
                            'text': DAWizard.messages.errors['not-available'],
                            'class': 'not-excempt'
                        },
                        'The DA WIzard displays development types that are exempt from requiring approval under the <em>Development Act 1993</em>.<br><br>From 1 July 2019, the new <em>Planning, Development and Infrastructure Act 2016</em> became operational in outback areas of South Australia, replacing the Development Act.',
                        'Learn more about <a href="' + DAWizard.config.url + '" title="visit Planning Reforms page">Implementation of the PDI Act in the outback</a>.',
                        false
                    )
                ).done(function( x ) {
                    $replaceThis=true;
			        0!=that.debug()&&(console.warn('DA WIzard not available for the address [Phase 1]'));
                });
                
			}
			else if(lgaIndex.Phase == 'Two'){
                a.when( 
                    that.displayResults(
                        {
                            'text': DAWizard.messages.errors['not-available'],
                            'class': 'not-excempt'
                        },
                        'The Development approval wizard displays development types that are exempt from requiring approval under the <em>Development Act 1993</em>.<br><br>From 1 July 2020, the new <em>Planning, Development and Infrastructure Act 2016</em> became operational in rural areas of South Australia, replacing the Development Act.',
                        'Learn more about <a href="' + DAWizard.config.url + '" title="visit Planning Reforms page">Implementation of the PDI Act in rural areas</a>.',
                        false
                    )
                ).done(function( x ) {
                    $replaceThis=true;
			        0!=that.debug()&&(console.warn('DA WIzard not available for the address [Phase 2]'));
                });
                
			}
			else if($replaceThis===true){
				
				a.when( 
                    that.displayResults(
                        {
                            'text':'You will need to apply for permission',
                            'class': 'not-excempt'
                        },
                        '',
                        '<span class="h4 mt-4 mb-2 d-block">Next Steps</span>' + DAWizard.messages.contactCouncilApply.replace(/%%LGA%%/g, $LayerJSON["LGA17url"]) + '',
                        'This property is located in ' + areaPlural + ' which requires a detailed assessment.' + statusMessage + ''
                    )
                ).done(function( x ) {
                    0!=that.debug()&&(console.warn('Determined application is required'));
                });
				
			}
			else if($selectedDevType==='other'){
				a.when( 
                    that.displayResults(
                        {
                            'text':'An approval may be required for the development types listed below.',
                            'class': 'not-excempt'
                        },
                        $currentPolicy,
                        '<span class="h4 mt-4 mb-2 d-block">Next Steps</span>' + DAWizard.messages.contactCouncilApply.replace(/%%LGA%%/g, $LayerJSON["LGA17url"]) + '',
                        false
                    )
                ).done(function( x ) {
                    0!=that.debug()&&(console.warn('Unable to determine'));
                });
				
			}else if($flagHistoric===true || $selectedDevType==='trees'){
				
				a.when( 
                    that.displayResults(
                        {
                            'text':'An approval may be required for this development.',
                            'class': 'not-excempt'
                        },
                        $currentPolicy + ' ' + footerDis,
                        '<span class="h4 mt-4 mb-2 d-block">Next Steps</span>' + DAWizard.messages.contactCouncilApply.replace(/%%LGA%%/g, $LayerJSON["LGA17url"]) + '',
                        false
                    )
                ).done(function( x ) {
                    0!=that.debug()&&(console.warn('Unable to determine'));
                });

			}else{
				a.when( 
                    that.displayResults(
                        {
                            'text':'It\'s unlikely you will need to apply for permission',
                            'class': 'excempt'
                        },
                        $currentPolicy + ' ' + footerDis,
                        '<span class="h4 mt-4 mb-2 d-block">Next Steps</span>' + DAWizard.messages.contactCouncilAdvice.replace(/%%LGA%%/g, $LayerJSON["LGA17url"]) + '',
                        false
                    )
                ).done(function( x ) {
                    0!=that.debug()&&(console.warn('Determined application not required if meet criteria'));
                });
			}
			0!=that.debug() && console.log('$flagHistoric: '+$flagHistoric+'\n$Zone23: '+$LayerJSON['Zone23']+'\n$exclusionZones: '+JSON.stringify($exclusionZones,null,2)+'\n$exclusionPolicies: '+JSON.stringify($exclusionPolicies,null,2));
			if($flagHistoric===true){
				if($localException===true){
				    $LayerJSON.ZoneMeaning23=policyOutbuilding;
				}
				0!=that.debug()&&(console.warn('Determined location has specific requirements'));
				a('body div#policy').find('li').each(function(i, el) {
					// higlight entire <li>
					a(el).wrapInTag({
                        tag: 'strong',
                        words: [$LayerJSON['ZoneMeaning23']]
					});
				});
			}
			
			if($highlightHidden!=false){
			    0!=that.debug() && console.log('$highlightHidden: ' + $highlightHidden);
			    /**
			     * only show if all 3 data-rel's match
			     * 
			     * data-rel-zone
			     * data-rel-policies
			     * data-rel-lga
			     *
			     **/
			     a('body #ih-zone #policy .d-none[data-rel-zone="' + $highlightHidden['data-rel-zone'] + '"][data-rel-policies="'+$highlightHidden['data-rel-policies']+'"][data-rel-lga="'+$highlightHidden['data-rel-lga']+'"]').removeClass('d-none');
			     $highlightHidden=false;
			}
			
			a('a[href*="#hovertext"],a[data-action="hovertext"],a[data-hover="hovertext"]').on('click', function(e){
			    e.preventDefault();
			});
			a('a[href*="#hovertext"],a[data-action="hovertext"],a[data-hover="hovertext"]').tooltip({
				delay: { show: 300, hide: 0 },
				placement: function(tip, element) { //$this is implicit
					var position = a(element).position();
					var topit = a(element).offset().top - a( '#ih' ).scrollTop();
					if (topit > 50) {
						return "top";
					}
					if (topit < 50){
						return "bottom";
					}
					//return "top";
				}
			});
			that.toggleElements('body #ih-zone', 'show', 400);
			a('body button.btn svg.svg-inline--fa').attr('role','presentation');
			0!=that.debug()&&(console.groupEnd('Matching Zones and policies'));
		},
		
		this.displayResults = function(title, message, nextStep, infoBlock){
		    // title = obj
		    var that = this;
		    if( typeof infoBlock == 'undefined' || infoBlock==false ){ infoBlock = '';var noShw = "d-none "; } else { var noShw = ""; };
		    a('body #ih #ih-zone')
		        .html("<div class='col-12 p-0'><div class='mb-4 alert-message-title " + title.class + "'>" + title.text + "</div><div><div class=\"" + noShw + "alert-wrapper\"><div class=\"alert alert-infoBlock\"><span>" + infoBlock + "</span></div></div>" + message + "</div><span id=\"council-contact\" class=\"my-5 d-block\">" + nextStep + "</span></div>"); 
		}
    },
    
    a(document).ready(function() {
        var DAWizardRun = new a.fn.DAWizard(DAWizard.config.VE);
        DAWizardRun.verifyTerms('load',DAWizard.config.url);
    })
}(jQuery);
// $(function(){
    var $debug = DAP.config.debug;
    $.fn.RunDAP = function(){
        var that = this;
        this.init = function(){
    		var that = this;
    		if($debug===true){
    		    console.group('debug [init]');
    		}
                
            $("body").on("change",'#dap_registration input,#dap_registration select', function() {
                var thisStep = $(this).removeClass('form-control-error').parents('.form-group').removeClass('form-group-error').find('.input-group span').html('');
            });
            
            var options = {
                url: function(phrase) {
                    return "//lsa1.geohub.sa.gov.au/arcgis/rest/services/Locators/SAGAF_Valuation/GeocodeServer/findAddressCandidates?Street=&Locality=&State=&Postcode=&category=&outFields=*&maxLocations=12&searchExtent=&location=&distance=&magicKey=&f=pjson&outSR=3857&Single+Line+Input="+phrase+"";
                },
                categories: [
                    { 
                        listLocation: "candidates",
                        maxNumberOfElements: 12,
                        match: {
                			enabled: false
                		}
                }],
                getValue: function(element) {
            		return element.address;
                },
            	
                list: {
                    maxNumberOfElements: 12,
                    match: {
                        enabled: true
                    },
                    sort: {
                        enabled: true
                    },
                    showAnimation: {
            			type: "slide", //normal|slide|fade
            			time: 400,
            			callback: function() {}
            		},
            		hideAnimation: {
            			type: "slide", //normal|slide|fade
            			time: 400,
            			callback: function() {}
            		},
                    onChooseEvent: function() {
            		    
            		    var selectedItemValue = $("#dap_registration #address-lookup").val();
            			var searchLocation = $("#dap_registration #address-lookup").getSelectedItemData().attributes;
            // 			console.log('RunDAP SALOC: '+JSON.stringify(searchLocation,null,2));
            			
            			// CREATE ADDRESS LINE
            			var Locality = searchLocation['Locality'],
            			    State = searchLocation['State'],
            			    Postcode = searchLocation['Postcode'],
            			    UnitType = searchLocation['UnitType'],
            			    UnitNumber = searchLocation['UnitNumber'],
            			    HouseNumber = searchLocation['HouseNumber'],
            			    StreetName = searchLocation['StreetName'],
            			    StreetType = searchLocation['StreetType'],
            			    StreetDir = searchLocation['StreetDir'],
            			    Match_addr = searchLocation['Match_addr'],
            			    Returned_addr = UnitType + ' ' + UnitNumber + ' ' + HouseNumber + ' ' + StreetName + ' ' + StreetType;
            			
            			$('#dap_registration #address-lookup').val(Match_addr); // ADDRESS LOOKUP
            			
            			if(Returned_addr.trim() === ""){
            			    $('#dap_registration input[name="' + DAP.FormParams.formQuestions['street'] + '"]').val(Match_addr); // ADDRESS LOOKUP
            			    var av = DAP.messages.addressInvalid.replace(/%%address%%/g, Match_addr);
            			}else{
            			    $('#dap_registration input[name="' + DAP.FormParams.formQuestions['street'] + '"]').val(Returned_addr.trim()); // ADDRESS
            			    // EXPAND FULL/MANUAL DETAILS
            			    $('#dap_registration button[data-action="show-address"]').trigger('click');
            			}
            			
            			$('#dap_registration input[name="' + DAP.FormParams.formQuestions['city'] + '"]').val(Locality); // CITY
            			$('#dap_registration select[name="' + DAP.FormParams.formQuestions['state'] + '"]').val(that.getKeyByValue(StateMap,State)); // STATE
            			$('#dap_registration input[name="' + DAP.FormParams.formQuestions['postcode'] + '"]').val(Postcode); // POSTCODE
            		}
                },
                
                // placeholder: DAP.messages.addressPlaceholder,
                
                template: {
                	type: "custom",
                		method: function(value, item) {
                			return value;
                		}
                },
                
                adjustWidth: false
            };
            
            $('#dap_registration #address-lookup').easyAutocomplete(options);
            
            if($debug === true){
                console.log('autocomplete enabled');
                console.groupEnd('debug [init]');
            }
        };
        
        this.getKeyByValue = function (object, value) {
            // return Object.keys(object).find(key => object[key] === value);
            return Object.keys(object)[Object.values(object).indexOf(value)];
        }
        
	};
	
	var RunDAP = new $.fn.RunDAP();
	RunDAP.init();
// 	$( '#address-lookup' ).focus();
    $('.easy-autocomplete').css({'width':'100%'});
    $('#dap_registration #address-lookup').attr('autocomplete','offplease');
    $('label#manual-street').next('small').text('Start typing to locate an address');
    $('body #breadcrumbs').remove();
    // update H1
    var theOldH1 = $('body h1').text();
    $('body h1').addClass('mt-5 mb-md-5');
    // page-heading mb-4 pt-2 mb-sm-5 mb-md-5 mb-lg-6
    // var theNewH1 = '<h1 class="page-heading mt-6 mb-4 mb-sm-5 mb-lg-6">'
    var theNewH1 = '<span class="h4 text-muted d-block my-0">' + DAP.messages.formH2 + '</span>'
            +'  <span class="h1">' + DAP.messages.formH1.yourself + '</span>';
        // +'</h1>';
    $('body h1').html(theNewH1).before('<button class="btn btn-back-link mt-2" onclick="window.history.go(-1)">Start again</button>');
    
    $('body button[data-action="show-address"]').removeAttr('disabled');
    $('body').on('click', 'button[data-action="show-address"]', function(e){
        e.preventDefault();
        $('#dap_registration #address-lookup, #dap_registration input[name="' + DAP.FormParams.formQuestions['street'] + '"], #dap_registration .easy-autocomplete').toggleClass('d-none'); 
        // Business request to not change these fields
        // $('#dap_registration div[data-rel="manual-address"] input').each(function(){
        //     $(this).val('');
        // });

        $('div[data-rel="manual-address"]').removeClass('d-none');
        $(this).text(DAP.messages.addressButtonSearch).attr('data-action','search-address');
        $('label#manual-street').next('small').text('Street');
        $('input[name="' + DAP.FormParams.formQuestions['street'] + '"]').attr('data-lookup','false');
        $('label#manual-street').attr('for','q619071_q8');
    });
    $('body').on('click', 'button[data-action="search-address"]', function(e){
        e.preventDefault();
        // Business request to not change these fields
        // $('#dap_registration #address-lookup').val('');
        // $('#dap_registration input[name="' + DAP.FormParams.formQuestions['street'] + '"], #dap_registration select[name="' + DAP.FormParams.formQuestions['state'] + '"]').val('');
        // $('#dap_registration select[name="' + DAP.FormParams.formQuestions['state'] + '"]').prop('selected', true);
        $('#dap_registration #address-lookup, #dap_registration input[name="' + DAP.FormParams.formQuestions['street'] + '"], #dap_registration .easy-autocomplete').toggleClass('d-none'); 
        
        $('div[data-rel="manual-address"]').addClass('d-none');
        $(this).text(DAP.messages.addressButton).attr('data-action','show-address');
        $('label#manual-street').next('small').text('Start typing to locate an address');
        $('input[name="' + DAP.FormParams.formQuestions['street'] + '"]').attr('data-lookup','true');
        $('label#manual-street').attr('for','address-lookup');
        
    });

    
    if($('#dap_registration input[name="' + DAP.FormParams.formQuestions['street'] + '"]').val() !=''){
        $('#dap_registration button[data-action="show-address"]').trigger('click');
    }
    // VALIDATION
    
    $('#' + DAP.FormParams.formSubmit).on('click', function(){
        $('#dap_registration button[data-action="show-address"]').trigger('click');
    });
    $("form#" + DAP.FormParams.formID).on('submit', function (e) {
        
        // radio label of selected to value
        $("input[type='radio']").each(function(){
            if(true === $debug){
                console.log("$(this).next('label').text(): " + $(this).next('label').text());
            }
            $(this).val($(this).next('label').text());
        });
        
        var payLoadIs =  $( "#" + DAP.FormParams.formID + " *" ).not(DAP.FormParams.formQuestions['form-page'] + ", input[name='" + DAP.FormParams.formQuestions['referral-url'] + "'], #g-recaptcha-response, #payloadb64, " + $("input[type='radio']").is(':not(:checked)')).serializeArray();
        
        // remove duplicates
        var jsonObject = payLoadIs.map(JSON.stringify); 
        var uniqueSet = new Set(jsonObject); 
        payLoadIs = Array.from(uniqueSet).map(JSON.parse);

        payLoadIs = JSON.stringify(payLoadIs);
        $('#payloadb64').val(btoa(payLoadIs));
        if(!validateAccRegReqtForm() || $debug === true){
            e.preventDefault();
            // scrollto form-group-error:first
            var target = $('.form-group-error').first();
            if(target.length>0){
                $('html, body').animate({
                    scrollTop: (target.offset().top - 55)
                }, 1000, "easeInOutExpo");
            }
            return false;
        } else {
            return true; 
        }
    });
    
    // Function to restrict alphacharacter
    // fails to allow for num-pad entering
    $('input[name="' + DAP.FormParams.formQuestions['phone-number'] + '"],input[name="' + DAP.FormParams.formQuestions['alt-phone'] + '"],input[name="' + DAP.FormParams.formQuestions['postcode'] + '"]').keypress(function(evt) {
        var numCharCode = (evt.which) ? evt.which : evt.keyCode;
        if (numCharCode > 31 && (numCharCode < 48 || numCharCode > 57)) {
            return false;
        }
        return true;
    }); 
    $('body .btn-back-link').find('svg.svg-inline--fa').attr('role','presentation');
    // force recaptcha callbacks
    
    $.when(
        $('body div.g-recaptcha').attr('data-callback','toggleSubmit').attr('data-expired-callback','disableSubmit')
    ).then(
        // disableSubmit()
    );

    function disableSubmit()
    {
        if($debug===true){
    	    console.log('debug [disableSubmit()]');
    	}
        var thisForm = $('#' + DAP.FormParams.formID + ' #' + DAP.FormParams.formSubmit);
            thisForm.attr('disabled', false).next('small').show();
            $('#form_recaptcha_message').show();
    };
    function toggleSubmit()
    {
        if($debug===true){
    	    console.group('debug [toggleSubmit()]');
    	}
        var thisForm = $('#' + DAP.FormParams.formID + ' #' + DAP.FormParams.formSubmit);
        // if (thisForm.attr('disabled')){
            if($debug===true){
        	    console.log('- enable form submit');
        	}
            thisForm.removeAttr('disabled').next('small').hide();
            $('#dap_registration button[data-action="show-address"]').trigger('click');
            $('#form_recaptcha_error, #form_recaptcha_message').hide();
        // }else{
        //     if($debug===true){
        // 	    console.log('- disable form submit');
        // 	}
        //     thisForm.attr('disabled', false).next('small').show();
        //     $('#form_recaptcha_error, #form_recaptcha_message').show();
        // }
        if($debug===true){
        	    console.groupEnd('debug [toggleSubmit()]');
        	}
    };
// });




function validateAccRegReqtForm()								 
{ 
// 	DAP.FormParams['formID']
//  DAP.FormParams.formQuestions['title'];
    // clear errors
    $('#val_error_msg').html('').addClass('d-none');
    $('.form-group-error').find('.form-control-error').removeClass('form-control-error');
    $('.form-group-error').removeClass('form-group-error').find('.error-message').html('');
	var title = document.forms[DAP.FormParams['formID']][DAP.FormParams.formQuestions['title']]; 
	var givenName = document.forms[DAP.FormParams['formID']][DAP.FormParams.formQuestions['given-name']]; 
	var familyName = document.forms[DAP.FormParams['formID']][DAP.FormParams.formQuestions['family-name']];			 
	var email = document.forms[DAP.FormParams['formID']][DAP.FormParams.formQuestions['email-address']]; 
	var phone = document.forms[DAP.FormParams['formID']][DAP.FormParams.formQuestions['phone-number']]; 
	var altphone = document.forms[DAP.FormParams['formID']][DAP.FormParams.formQuestions['alt-phone']];
	var contmethod = document.forms[DAP.FormParams['formID']][DAP.FormParams.formQuestions['contact-method']];
	var street = document.forms[DAP.FormParams['formID']][DAP.FormParams.formQuestions['street']];
	var state = document.forms[DAP.FormParams['formID']][DAP.FormParams.formQuestions['state']];
	var city = document.forms[DAP.FormParams['formID']][DAP.FormParams.formQuestions['city']];
	var postcode = document.forms[DAP.FormParams['formID']][DAP.FormParams.formQuestions['postcode']];
    
    var validated = true;
    
    // To check empty form fields.
    
    // Check each input in the order that it appears in the form.
    if (!trueSelection(DAP.FormParams.formQuestions['title'], "Please select your title","select")) {
        validated = false;
    }
    if (!inputAlphabet(givenName, "For your Given name please use alphabets only",'Given name')) {
        validated = false;
    }
    if (!lengthDefine(givenName, "Please enter between 1 and 35 characters", 1, 35)) {
        validated = false;
    }
    if (!inputAlphabet(familyName, "For your Family name please use alphabets only",'Family name')) {
        validated = false;
    }
    if (!lengthDefine(familyName, "Please enter between 1 and 35 characters", 1, 35)) {
        validated = false;
    }
    if (!emailValidation(email, "Please enter a valid email address")) {
        validated = false;
    }
    if (!textNumeric(phone, "Please enter a valid phone number", "true")) {
        validated = false;
    }
    if (!textNumeric(altphone, "Please enter a valid alternate phone number","false")) {
        validated = false;
    }
    
    if (!inputAlphabet(city, "For the City please use alphabets only",'City')) {
        validated = false;
    }
    if (!trueSelection(state, "Please select an option for State",'select')) {
        validated = false;
    }
    
    if (!inputPostcode(postcode, "Please enter a valid postcode",'Postcode')) {
        validated = false;
    }
    
    // Check each input in the order that it appears in the form.
    if (!trueSelection(DAP.FormParams.formQuestions['contact-method'], "Please select your preferred method","radio")) {
        validated = false;
    }
    
    return validated;
}

// Function that checks whether input text is numeric or not.
function textNumeric(inputtext, alertMsg, required) {
    if (typeof required === "undefined"){
        required = "true";
    }
    if ("" === inputtext.value && "false" === required) {
        return true;
    }
    // var numericExpression = /^\({0,1}((0|1)(2|4|3|7|8)){0,1}\){0,1}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{1}(\ |-){0,1}[0-9]{3}$/; //  /^[0-9]+$/
    var numericExpression = /(^(0?\d|\(0?\d\))?[ \-]*?(([ \-]*?\d){8})$)|(^(\+\d{1,3}[ \-]*?(([ \-]*?\d){5,15}))$)/;
    if (inputtext.value.match(numericExpression)) {
        if(DAP.config.debug===true){ console.log('textNumeric [validate=true] ' + alertMsg); }
        return true;    
    } else {
        if(DAP.config.debug===true){ console.warn('textNumeric [validate=false] ' + alertMsg); }
        $("#" + inputtext.id + "_error").html(alertMsg);
        $("#" + inputtext.id).addClass('form-control-error').parents('div.form-group').addClass('form-group-error');

        return false;
    }
}
// Function that checks whether input text is an alphabetic character or not.
function inputAlphabet(inputtext, alertMsg, field) {
    var alphaExp = /^[A-Za-z-_\s']*$/;
    inputtext.value = inputtext.value.trim();
    $("#" + inputtext.id).val(inputtext.value);
    if (inputtext.value.match(alphaExp)) {
        if(DAP.config.debug===true){ console.log('inputAlphabet [validate=true] ' + alertMsg); }
        return true;
    } else {
        if(DAP.config.debug===true){ console.warn('inputAlphabet [validate=false] ' + alertMsg); }
        $("#" + inputtext.id + "_error").html(alertMsg);
        $("#" + inputtext.id).addClass('form-control-error').parents('div.form-group').addClass('form-group-error');
        
        return false;
    }
}
// Function that checks whether input text is numeric or not for Postcode.
function inputPostcode(inputtext, alertMsg, field) {
    var numericPostcodeExp = /^\d{4}$/;
    if (inputtext.value.match(numericPostcodeExp)) {
        if(DAP.config.debug===true){ console.log('inputPostcode [validate=true] ' + alertMsg); }
        return true;
    } else {
        if(DAP.config.debug===true){ console.warn('inputPostcode [validate=false] ' + alertMsg); }
        $("#" + inputtext.id + "_error").html(alertMsg);
        $("#" + inputtext.id).addClass('form-control-error').parents('div.form-group').addClass('form-group-error');

        return false;
    }
}
// Function that checks whether the input characters are restricted according to defined by user.
function lengthDefine(inputtext, alertMsg, min, max) {
    var uInput = inputtext.value;
    if (uInput.length >= min && uInput.length <= max) {
        if(DAP.config.debug===true){ console.log('lengthDefine [validate=true] ' + alertMsg); }
        return true;
    } else {
        if(DAP.config.debug===true){ console.warn('lengthDefine [validate=false] ' + alertMsg); }
        $("#" + inputtext.id + "_error").html(alertMsg);
        $("#" + inputtext.id).addClass('form-control-error').parents('div.form-group').addClass('form-group-error');

        return false;
    }
}
// Function that checks whether a option is selected from the selector and if it's not it displays an alert message.
function trueSelection(inputtext, alertMsg, type) {
    switch(type){
        case 'radio':
            if (!$("input[name='" + inputtext + "']:checked").val()) {
                $("input[name='" + inputtext + "']").parents('div.form-group').addClass('form-group-error').find('.error-message').html(alertMsg);
                if(DAP.config.debug===true){ console.warn('trueSelection [validate[radio]=false] ' + alertMsg); }
                return false;
            } else {
                if(DAP.config.debug===true){ console.log('trueSelection [validate[radio]=true] ' + alertMsg); }
                return true;
            }
            break;
        default:
        case 'select':
            if (inputtext.value == "-- Please Select --" || inputtext.value == "") {
                $("#" + inputtext.id + "_error").html(alertMsg);
                $("#" + inputtext.id).addClass('form-control-error').parents('div.form-group').addClass('form-group-error');
                if(DAP.config.debug===true){ console.warn('trueSelection [validate[select]=false] ' + alertMsg); }
                return false;
            } else {
                if(DAP.config.debug===true){ console.log('trueSelection [validate[select]=true] ' + alertMsg); }
                return true;
            }
            break;
    }
}
// Function that checks whether an user entered valid email address or not and displays alert message on wrong email address format.
function emailValidation(inputtext, alertMsg) {
    var emailExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; // /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/
    if (inputtext.value.match(emailExp)) {
        if(DAP.config.debug===true){ console.log('emailValidation [validate=true] ' + alertMsg); }
        return true;
    } else {
        if(DAP.config.debug===true){ console.warn('emailValidation [validate=false] ' + alertMsg); }
        $("#" + inputtext.id + "_error").html(alertMsg);
        $("#" + inputtext.id).addClass('form-control-error').parents('div.form-group').addClass('form-group-error');

        return false;
    }
}
